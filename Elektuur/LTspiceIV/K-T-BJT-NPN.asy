Version 4
SymbolType CELL K-T-BJT-NPN
SYMATTR Description NPN bipolar junction transistor (BJT)
LINE Normal 16 -32 16 -22
LINE Normal 16 -22 -1 -5
LINE Normal -32 0 -6 0
LINE Normal 16 32 16 22
LINE Normal 16 22 -1 5
LINE Normal 11 17 10 15
LINE Normal 10 15 9 16
LINE Normal 9 16 11 17
LINE Normal 10 14 8 16
LINE Normal 10 13 7 16
LINE Normal 9 13 7 15
LINE Normal 9 12 6 15
LINE Normal 9 11 5 15
LINE Normal 12 18 9 10
LINE Normal 9 10 4 15
LINE Normal 4 15 12 18
RECTANGLE Normal -4 -15 -3 15
RECTANGLE Normal -5 -16 -2 16
RECTANGLE Normal -6 -17 -1 17
SYMATTR Prefix QN
WINDOW 0 24 -32 Left 1
SYMATTR InstName T
SYMATTR Value NPN
WINDOW 3 24 32 Left 1
PIN 16 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -32 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 16 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
