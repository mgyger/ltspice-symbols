Version 4
SymbolType CELL K-D
SYMATTR Description semiconductor diode
LINE Normal -32 0 32 0
LINE Normal 5 1 5 -1
LINE Normal 4 2 4 -2
LINE Normal 3 2 3 -2
LINE Normal 2 3 2 -3
LINE Normal 1 4 1 -4
LINE Normal 0 4 0 -4
LINE Normal -1 5 -1 -5
LINE Normal -2 6 -2 -6
LINE Normal -3 6 -3 -6
LINE Normal -4 7 -4 -7
LINE Normal -5 8 -5 -8
LINE Normal -6 8 -6 -8
LINE Normal -7 9 -7 -9
LINE Normal -8 10 -8 -10
LINE Normal -9 10 -9 -10
LINE Normal -10 11 -10 -11
LINE Normal -11 12 -11 -12
LINE Normal -12 12 -12 -12
LINE Normal -13 13 -13 -13
LINE Normal -14 14 -14 -14
LINE Normal 7 0 -15 15
LINE Normal -15 15 -15 -15
LINE Normal -15 -15 7 0
LINE Normal 12 12 12 -12
RECTANGLE Normal 11 13 13 -13
RECTANGLE Normal 10 14 14 -14
RECTANGLE Normal 9 15 15 -15
SYMATTR Prefix D
WINDOW 0 -24 -8 VLeft 1
SYMATTR Value D
WINDOW 3 24 -8 VLeft 1
PIN -32 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
