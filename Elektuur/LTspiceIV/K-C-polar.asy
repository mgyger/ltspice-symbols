Version 4
SymbolType CELL K-C-polar
SYMATTR Description capacitor, polar(ized)
LINE Normal -32 0 -12 0
LINE Normal 10 0 32 0
RECTANGLE Normal -11 14 -4 -14
RECTANGLE Normal -12 15 -3 -15
LINE Normal 7 12 7 -12
RECTANGLE Normal 6 13 8 -13
RECTANGLE Normal 5 14 9 -14
RECTANGLE Normal 4 15 10 -15
SYMATTR 162 +
WINDOW 162 -24 12 Invisible 1
SYMATTR Prefix C
WINDOW 0 -24 -8 VLeft 1
SYMATTR Value C
WINDOW 3 24 -8 VLeft 1
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
