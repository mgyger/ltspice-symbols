Version 4
SymbolType CELL K-L
SYMATTR Description coil/winding/inductor/choke/reactor\nwinding with instantaneous voltage polarity indicator/dot
LINE Normal -32 0 -25 0
LINE Normal -13 0 -12 0
LINE Normal 0 0 1 0
LINE Normal 13 0 14 0
LINE Normal 26 0 32 0
ARC Normal -25 6 -13 -6 -13 0 -25 0
ARC Normal -12 6 0 -6 0 0 -12 0
ARC Normal 1 6 13 -6 13 0 1 0
ARC Normal 14 6 26 -6 26 0 14 0
ARC Normal -26 7 -12 -7 -12 0 -26 0
ARC Normal -13 7 1 -7 1 0 -13 0
ARC Normal 0 7 14 -7 14 0 0 0
ARC Normal 13 7 27 -7 27 0 13 0
SYMATTR 162 �
WINDOW 162 -21 0 VCenter 1
SYMATTR Prefix L
WINDOW 0 -24 -8 VLeft 1
SYMATTR Value L
WINDOW 3 24 -12 VLeft 1
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
