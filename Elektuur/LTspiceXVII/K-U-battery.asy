Version 4
SymbolType CELL K-U-battery
SYMATTR Description primary cell; secondary/rechargeable cell; battery of primary or secondary cells; DC supply
LINE Normal -32 0 -7 0
LINE Normal 8 0 32 0
LINE Normal -6 30 -6 -30
RECTANGLE Normal -7 31 -5 -31
LINE Normal 5 12 5 -12
RECTANGLE Normal 4 13 6 -13
RECTANGLE Normal 3 14 7 -14
RECTANGLE Normal 2 15 8 -15
SYMATTR 162 +
WINDOW 162 -24 16 VCenter 1
SYMATTR Prefix V
WINDOW 0 -24 -8 VLeft 1
SYMATTR InstName U
SYMATTR Value V
WINDOW 3 24 -8 VLeft 1
SYMATTR SpiceLine Rser=0.1
WINDOW 39 24 8 VRight 1
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
