Version 4
SymbolType CELL K-R-LDR
SYMATTR Description light dependent resistor (LDR); photo resistor
LINE Normal -32 0 -27 0
LINE Normal 27 0 32 0
LINE Normal -24 9 -24 -9
LINE Normal -23 8 -23 -8
LINE Normal -22 7 -22 -7
LINE Normal -21 6 -21 -6
LINE Normal -20 5 -20 -5
LINE Normal -19 4 -19 -4
LINE Normal -18 3 -18 -3
LINE Normal -17 2 -17 -2
LINE Normal -16 1 -16 -1
LINE Normal -25 10 -15 0
LINE Normal -15 0 -25 -10
LINE Normal 24 9 24 -9
LINE Normal 23 8 23 -8
LINE Normal 22 7 22 -7
LINE Normal 21 6 21 -6
LINE Normal 20 5 20 -5
LINE Normal 19 4 19 -4
LINE Normal 18 3 18 -3
LINE Normal 17 2 17 -2
LINE Normal 16 1 16 -1
LINE Normal 25 10 15 0
LINE Normal 15 0 25 -10
RECTANGLE Normal -25 10 25 -10
RECTANGLE Normal -26 11 26 -11
RECTANGLE Normal -27 12 27 -12
LINE Normal -12 23 -12 41
LINE Normal -12 26 -13 30
LINE Normal -13 30 -11 30
LINE Normal -11 30 -12 26
LINE Normal -12 25 -14 31
LINE Normal -14 31 -10 31
LINE Normal -10 31 -12 25
LINE Normal -12 24 -15 32
LINE Normal -15 32 -9 32
LINE Normal -9 32 -12 24
LINE Normal -12 23 -16 33
LINE Normal -16 33 -8 33
LINE Normal -8 33 -12 23
LINE Normal 0 23 0 41
LINE Normal 0 26 -1 30
LINE Normal -1 30 1 30
LINE Normal 1 30 0 26
LINE Normal 0 25 -2 31
LINE Normal -2 31 2 31
LINE Normal 2 31 0 25
LINE Normal 0 24 -3 32
LINE Normal -3 32 3 32
LINE Normal 3 32 0 24
LINE Normal 0 23 -4 33
LINE Normal -4 33 4 33
LINE Normal 4 33 0 23
LINE Normal 12 23 12 41
LINE Normal 12 26 11 30
LINE Normal 11 30 13 30
LINE Normal 13 30 12 26
LINE Normal 12 25 10 31
LINE Normal 10 31 14 31
LINE Normal 14 31 12 25
LINE Normal 12 24 9 32
LINE Normal 9 32 15 32
LINE Normal 15 32 12 24
LINE Normal 12 23 8 33
LINE Normal 8 33 16 33
LINE Normal 16 33 12 23
SYMATTR 162 +
WINDOW 162 -24 24 Invisible 1
SYMATTR Prefix R
WINDOW 0 -40 -8 VLeft 1
SYMATTR Value R
WINDOW 3 40 -8 VLeft 1
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
