Version 4
SymbolType CELL K-D-Zener
SYMATTR Description breakdown diode, unidirectional; Zener/avalanche diode; voltage regulator diode
LINE Normal -32 0 -21 0
LINE Normal 21 0 32 0
LINE Normal -1 0 21 15
LINE Normal -3 0 19 15
LINE Normal 1 0 20 13
RECTANGLE Normal 19 15 21 13
LINE Normal 19 11 19 -13
RECTANGLE Normal 18 12 20 -14
RECTANGLE Normal 17 13 21 -15
LINE Normal -1 1 -1 -1
LINE Normal -2 2 -2 -2
LINE Normal -3 2 -3 -2
LINE Normal -4 3 -4 -3
LINE Normal -5 4 -5 -4
LINE Normal -6 4 -6 -4
LINE Normal -7 5 -7 -5
LINE Normal -8 6 -8 -6
LINE Normal -9 6 -9 -6
LINE Normal -10 7 -10 -7
LINE Normal -11 8 -11 -8
LINE Normal -12 8 -12 -8
LINE Normal -13 9 -13 -9
LINE Normal -14 10 -14 -10
LINE Normal -15 10 -15 -10
LINE Normal -16 11 -16 -11
LINE Normal -17 12 -17 -12
LINE Normal -18 12 -18 -12
LINE Normal -19 13 -19 -13
LINE Normal -20 14 -20 -14
LINE Normal 1 0 -21 15
LINE Normal -21 15 -21 -15
LINE Normal -21 -15 1 0
SYMATTR Prefix D
WINDOW 0 -24 -24 VLeft 1
SYMATTR Value D
WINDOW 3 24 -24 VLeft 1
PIN -32 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
