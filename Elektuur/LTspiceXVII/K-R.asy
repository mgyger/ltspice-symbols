Version 4
SymbolType CELL K-R
SYMATTR Description resistor
LINE Normal -32 0 -27 0
LINE Normal 27 0 32 0
RECTANGLE Normal -25 10 25 -10
RECTANGLE Normal -26 11 26 -11
RECTANGLE Normal -27 12 27 -12
SYMATTR 162 +
WINDOW 162 -24 24 Invisible 1
SYMATTR Prefix R
WINDOW 0 -40 -8 VLeft 1
SYMATTR Value R
WINDOW 3 0 0 Center 1
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
