Version 4
SymbolType CELL K-X-lamp
SYMATTR Description lamp
LINE Normal -32 0 -15 0
LINE Normal 15 0 32 0
LINE Normal -9 9 9 -9
LINE Normal -9 -9 9 9
CIRCLE Normal -13 13 13 -13
CIRCLE Normal -14 14 14 -14
CIRCLE Normal -15 15 15 -15
SYMATTR Prefix X
WINDOW 0 -24 -8 VLeft 1
SYMATTR InstName La
SYMATTR Value subckt
WINDOW 3 24 -8 VLeft 1
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
