Version 4
SymbolType CELL K-F
SYMATTR Description fuse
LINE Normal -32 0 32 0
LINE Normal -25 10 -25 -10
LINE Normal -25 -10 25 -10
LINE Normal 25 -10 25 10
LINE Normal 25 10 -25 10
LINE Normal -26 11 -26 -11
LINE Normal -26 -11 26 -11
LINE Normal 26 -11 26 11
LINE Normal 26 11 -26 11
LINE Normal -27 12 -27 -12
LINE Normal -27 -12 27 -12
LINE Normal 27 -12 27 12
LINE Normal 27 12 -27 12
SYMATTR 162 +
WINDOW 162 -24 24 Invisible 1
SYMATTR Prefix X
WINDOW 0 -40 -8 VLeft 1
SYMATTR InstName F
SYMATTR SpiceModel subckt
WINDOW 38 40 -8 VLeft 1
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
