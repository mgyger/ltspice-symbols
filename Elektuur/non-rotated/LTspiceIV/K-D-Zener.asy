Version 4
SymbolType CELL K-D-Zener
SYMATTR Description breakdown diode, unidirectional; Zener/avalanche diode; voltage regulator diode
LINE Normal 0 -32 0 -21
LINE Normal 0 21 0 32
LINE Normal 0 -1 -15 21
LINE Normal 0 -3 -15 19
LINE Normal 0 1 -13 20
RECTANGLE Normal -15 19 -13 21
LINE Normal -11 19 13 19
RECTANGLE Normal -12 18 14 20
RECTANGLE Normal -13 17 15 21
LINE Normal -1 -1 1 -1
LINE Normal -2 -2 2 -2
LINE Normal -2 -3 2 -3
LINE Normal -3 -4 3 -4
LINE Normal -4 -5 4 -5
LINE Normal -4 -6 4 -6
LINE Normal -5 -7 5 -7
LINE Normal -6 -8 6 -8
LINE Normal -6 -9 6 -9
LINE Normal -7 -10 7 -10
LINE Normal -8 -11 8 -11
LINE Normal -8 -12 8 -12
LINE Normal -9 -13 9 -13
LINE Normal -10 -14 10 -14
LINE Normal -10 -15 10 -15
LINE Normal -11 -16 11 -16
LINE Normal -12 -17 12 -17
LINE Normal -12 -18 12 -18
LINE Normal -13 -19 13 -19
LINE Normal -14 -20 14 -20
LINE Normal 0 1 -15 -21
LINE Normal -15 -21 15 -21
LINE Normal 15 -21 0 1
SYMATTR Prefix D
WINDOW 0 24 -24 Left 1
SYMATTR Value D
WINDOW 3 24 24 Left 1
PIN 0 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
