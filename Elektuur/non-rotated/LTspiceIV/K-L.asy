Version 4
SymbolType CELL K-L
SYMATTR Description coil/winding/inductor/choke/reactor\nwinding with instantaneous voltage polarity indicator/dot
LINE Normal 0 -32 0 -25
LINE Normal 0 -13 0 -12
LINE Normal 0 0 0 1
LINE Normal 0 13 0 14
LINE Normal 0 26 0 32
ARC Normal -6 -25 6 -13 0 -13 0 -25
ARC Normal -6 -12 6 0 0 0 0 -12
ARC Normal -6 1 6 13 0 13 0 1
ARC Normal -6 14 6 26 0 26 0 14
ARC Normal -7 -26 7 -12 0 -12 0 -26
ARC Normal -7 -13 7 1 0 1 0 -13
ARC Normal -7 0 7 14 0 14 0 0
ARC Normal -7 13 7 27 0 27 0 13
SYMATTR 162 �
WINDOW 162 0 -21 Center 1
SYMATTR Prefix L
WINDOW 0 8 -24 Left 1
SYMATTR Value L
WINDOW 3 12 24 Left 1
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
