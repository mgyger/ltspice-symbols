Version 4
SymbolType CELL K-A-comp
SYMATTR Description voltage comparator; operational amplifier (opamp)
LINE Normal 44 0 -31 -43
LINE Normal -30 -43 -30 43
LINE Normal -31 43 44 0
LINE Normal 46 0 -31 -44
LINE Normal -31 -44 -31 44
LINE Normal -31 44 46 0
LINE Normal 48 0 -32 -46
LINE Normal -32 -46 -32 46
LINE Normal -32 46 48 0
LINE Normal 0 -32 0 -27
LINE Normal 0 27 0 32
SYMATTR 162 -
WINDOW 162 -20 16 Center 1
SYMATTR 163 +
WINDOW 163 -20 -16 Center 1
SYMATTR 164 -
WINDOW 164 0 16 Center 1
SYMATTR 165 +
WINDOW 165 0 -16 Center 1
SYMATTR Prefix X
WINDOW 0 0 0 Center 1
SYMATTR InstName A
SYMATTR SpiceModel subckt
WINDOW 38 8 40 Left 1
PIN -32 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 0 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN 0 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
PIN 48 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 5
