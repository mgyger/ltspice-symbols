Version 4
SymbolType CELL K-F
SYMATTR Description fuse
LINE Normal 0 -32 0 32
LINE Normal -10 -25 10 -25
LINE Normal 10 -25 10 25
LINE Normal 10 25 -10 25
LINE Normal -10 25 -10 -25
LINE Normal -11 -26 11 -26
LINE Normal 11 -26 11 26
LINE Normal 11 26 -11 26
LINE Normal -11 26 -11 -26
LINE Normal -12 -27 12 -27
LINE Normal 12 -27 12 27
LINE Normal 12 27 -12 27
LINE Normal -12 27 -12 -27
SYMATTR 162 +
WINDOW 162 -24 -24 Invisible 1
SYMATTR Prefix X
WINDOW 0 8 -40 Left 1
SYMATTR InstName F
SYMATTR SpiceModel subckt
WINDOW 38 8 40 Left 1
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
