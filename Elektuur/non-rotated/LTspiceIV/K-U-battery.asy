Version 4
SymbolType CELL K-U-battery
SYMATTR Description primary cell; secondary/rechargeable cell; battery of primary or secondary cells; DC supply
LINE Normal 0 -32 0 -7
LINE Normal 0 8 0 32
LINE Normal -30 -6 30 -6
RECTANGLE Normal -31 -7 31 -5
LINE Normal -12 5 12 5
RECTANGLE Normal -13 4 13 6
RECTANGLE Normal -14 3 14 7
RECTANGLE Normal -15 2 15 8
SYMATTR 162 +
WINDOW 162 -16 -24 Center 1
SYMATTR Prefix V
WINDOW 0 8 -24 Left 1
SYMATTR InstName U
SYMATTR Value V
WINDOW 3 8 24 Left 1
SYMATTR SpiceLine Rser=0.1
WINDOW 39 -8 24 Right 1
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
