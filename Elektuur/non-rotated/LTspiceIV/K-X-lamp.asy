Version 4
SymbolType CELL K-X-lamp
SYMATTR Description lamp
LINE Normal 0 -32 0 -15
LINE Normal 0 15 0 32
LINE Normal -9 -9 9 9
LINE Normal 9 -9 -9 9
CIRCLE Normal -13 -13 13 13
CIRCLE Normal -14 -14 14 14
CIRCLE Normal -15 -15 15 15
SYMATTR Prefix X
WINDOW 0 8 -24 Left 1
SYMATTR InstName La
SYMATTR Value subckt
WINDOW 3 8 24 Left 1
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
