Version 4
SymbolType CELL K-R-LDR
SYMATTR Description light dependent resistor (LDR); photo resistor
LINE Normal 0 -32 0 -27
LINE Normal 0 27 0 32
LINE Normal -9 -24 9 -24
LINE Normal -8 -23 8 -23
LINE Normal -7 -22 7 -22
LINE Normal -6 -21 6 -21
LINE Normal -5 -20 5 -20
LINE Normal -4 -19 4 -19
LINE Normal -3 -18 3 -18
LINE Normal -2 -17 2 -17
LINE Normal -1 -16 1 -16
LINE Normal -10 -25 0 -15
LINE Normal 0 -15 10 -25
LINE Normal -9 24 9 24
LINE Normal -8 23 8 23
LINE Normal -7 22 7 22
LINE Normal -6 21 6 21
LINE Normal -5 20 5 20
LINE Normal -4 19 4 19
LINE Normal -3 18 3 18
LINE Normal -2 17 2 17
LINE Normal -1 16 1 16
LINE Normal -10 25 0 15
LINE Normal 0 15 10 25
RECTANGLE Normal -10 -25 10 25
RECTANGLE Normal -11 -26 11 26
RECTANGLE Normal -12 -27 12 27
LINE Normal -23 -12 -41 -12
LINE Normal -26 -12 -30 -13
LINE Normal -30 -13 -30 -11
LINE Normal -30 -11 -26 -12
LINE Normal -25 -12 -31 -14
LINE Normal -31 -14 -31 -10
LINE Normal -31 -10 -25 -12
LINE Normal -24 -12 -32 -15
LINE Normal -32 -15 -32 -9
LINE Normal -32 -9 -24 -12
LINE Normal -23 -12 -33 -16
LINE Normal -33 -16 -33 -8
LINE Normal -33 -8 -23 -12
LINE Normal -23 0 -41 0
LINE Normal -26 0 -30 -1
LINE Normal -30 -1 -30 1
LINE Normal -30 1 -26 0
LINE Normal -25 0 -31 -2
LINE Normal -31 -2 -31 2
LINE Normal -31 2 -25 0
LINE Normal -24 0 -32 -3
LINE Normal -32 -3 -32 3
LINE Normal -32 3 -24 0
LINE Normal -23 0 -33 -4
LINE Normal -33 -4 -33 4
LINE Normal -33 4 -23 0
LINE Normal -23 12 -41 12
LINE Normal -26 12 -30 11
LINE Normal -30 11 -30 13
LINE Normal -30 13 -26 12
LINE Normal -25 12 -31 10
LINE Normal -31 10 -31 14
LINE Normal -31 14 -25 12
LINE Normal -24 12 -32 9
LINE Normal -32 9 -32 15
LINE Normal -32 15 -24 12
LINE Normal -23 12 -33 8
LINE Normal -33 8 -33 16
LINE Normal -33 16 -23 12
SYMATTR 162 +
WINDOW 162 -24 -24 Invisible 1
SYMATTR Prefix R
WINDOW 0 8 -40 Left 1
SYMATTR Value R
WINDOW 3 8 40 Left 1
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
