Version 4
SymbolType CELL K-R
SYMATTR Description resistor
LINE Normal 0 -32 0 -27
LINE Normal 0 27 0 32
RECTANGLE Normal -10 -25 10 25
RECTANGLE Normal -11 -26 11 26
RECTANGLE Normal -12 -27 12 27
SYMATTR 162 +
WINDOW 162 -24 -24 Invisible 1
SYMATTR Prefix R
WINDOW 0 8 -40 Left 1
SYMATTR Value R
WINDOW 3 0 0 VCenter 1
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
