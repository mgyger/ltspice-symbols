Version 4
SymbolType GRAPHIC K-T-BJT-envelope
SYMATTR Description envelope, for bipolar junction transistor (BJT)
CIRCLE Normal -21 -25 29 25
CIRCLE Normal -22 -26 30 26
CIRCLE Normal -23 -27 31 27
