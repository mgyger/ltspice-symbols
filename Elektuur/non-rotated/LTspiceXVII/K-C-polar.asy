Version 4
SymbolType CELL K-C-polar
SYMATTR Description capacitor, polar(ized)
LINE Normal 0 -32 0 -12
LINE Normal 0 10 0 32
RECTANGLE Normal -14 -11 14 -4
RECTANGLE Normal -15 -12 15 -3
LINE Normal -12 7 12 7
RECTANGLE Normal -13 6 13 8
RECTANGLE Normal -14 5 14 9
RECTANGLE Normal -15 4 15 10
SYMATTR 162 +
WINDOW 162 -12 -24 Invisible 1
SYMATTR Prefix C
WINDOW 0 8 -24 Left 1
SYMATTR Value C
WINDOW 3 8 24 Left 1
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
