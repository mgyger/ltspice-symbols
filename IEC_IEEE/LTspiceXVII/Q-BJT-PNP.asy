Version 4
SymbolType CELL Q-BJT-PNP
SYMATTR Description PNP bipolar junction transistor (BJT)
LINE Normal -32 0 0 0
LINE Normal 32 32 0 8
LINE Normal 32 -32 0 -8
LINE Normal 0 -16 0 16
LINE Normal 20 -17 8 -14
LINE Normal 8 -14 14 -25
SYMATTR Prefix QP
WINDOW 0 48 -16 Left 2
SYMATTR Value PNP
WINDOW 3 48 16 Left 2
PIN 32 32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -32 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 32 -32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
