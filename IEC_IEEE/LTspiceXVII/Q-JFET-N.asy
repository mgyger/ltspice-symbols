Version 4
SymbolType CELL Q-JFET-N
SYMATTR Description N-channel junction-gate field-effect transistor (JFET)
LINE Normal 0 -16 32 -16
LINE Normal 32 -16 32 -32
LINE Normal -32 16 32 16
LINE Normal 32 16 32 32
LINE Normal 0 -24 0 24
LINE Normal -18 11 -6 16
LINE Normal -6 16 -18 21
SYMATTR Prefix JN
WINDOW 0 48 -16 Left 2
SYMATTR Value NJF
WINDOW 3 48 16 Left 2
PIN 32 -32 None 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName S
PINATTR SpiceOrder 3
