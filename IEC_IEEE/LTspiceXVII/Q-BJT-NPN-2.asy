Version 4
SymbolType CELL Q-BJT-NPN-2
SYMATTR Description dual (matched-pair) NPN bipolar junction transistor (BJT)
RECTANGLE Normal -80 -32 80 32
LINE Normal -80 0 -48 0
LINE Normal -16 32 -48 8
LINE Normal -16 -32 -48 -8
LINE Normal -48 -16 -48 16
LINE Normal -40 20 -28 23
LINE Normal -28 23 -34 12
LINE Normal 80 0 48 0
LINE Normal 16 32 48 8
LINE Normal 16 -32 48 -8
LINE Normal 48 -16 48 16
LINE Normal 40 20 28 23
LINE Normal 28 23 34 12
SYMATTR Prefix X
WINDOW 0 88 -16 Left 2
SYMATTR InstName Q
SYMATTR SpiceModel MAT02
WINDOW 38 88 16 Left 2
SYMATTR ModelFile Q-BJT-NPN-2-ADI.lib
PIN -16 -32 None 8
PINATTR PinName C1
PINATTR SpiceOrder 1
PIN -80 0 None 8
PINATTR PinName B1
PINATTR SpiceOrder 2
PIN -16 32 None 8
PINATTR PinName E1
PINATTR SpiceOrder 3
PIN 16 32 None 8
PINATTR PinName E2
PINATTR SpiceOrder 4
PIN 80 0 None 8
PINATTR PinName B2
PINATTR SpiceOrder 5
PIN 16 -32 None 8
PINATTR PinName C2
PINATTR SpiceOrder 6
