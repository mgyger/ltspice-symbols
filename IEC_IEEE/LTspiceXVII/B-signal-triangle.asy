Version 4
SymbolType GRAPHIC B-signal-triangle
SYMATTR Description triangle wave (scaled)
LINE Normal -8 12 -4 4
LINE Normal -4 4 0 12
LINE Normal 0 12 4 4
LINE Normal 4 4 8 12
