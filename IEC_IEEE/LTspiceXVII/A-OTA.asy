V e r s i o n   4  
 S y m b o l T y p e   C E L L   A - O T A  
 S Y M A T T R   D e s c r i p t i o n   m u l t i p l i e r   w i t h   c o m p l e m e n t a r y   i n p u t s   a n d   o p e r a t i o n a l   t r a n s c o n d u c t a n c e   a m p l i f i e r   ( O T A )  
 R E C T A N G L E   N o r m a l   - 3 2   - 4 8   3 2   4 8  
 L I N E   N o r m a l   - 1 6   - 3 2   - 1 6   - 1 6  
 L I N E   N o r m a l   - 1 6   1 6   - 1 6   3 2  
 S Y M A T T R   1 6 2   " 
 W I N D O W   1 6 2   - 2 4   - 3 2   C e n t e r   1  
 S Y M A T T R   1 6 3   +  
 W I N D O W   1 6 3   - 2 4   - 1 6   C e n t e r   1  
 S Y M A T T R   1 6 4   +  
 W I N D O W   1 6 4   - 2 4   1 6   C e n t e r   1  
 S Y M A T T R   1 6 5   " 
 W I N D O W   1 6 5   - 2 4   3 2   C e n t e r   1  
 S Y M A T T R   1 6 6    / I  
 W I N D O W   1 6 6   0   0   C e n t e r   2  
 W I N D O W   3   0   6 4   C e n t e r   2  
 S Y M A T T R   P r e f i x   A  
 W I N D O W   0   0   - 6 4   C e n t e r   2  
 S Y M A T T R   S p i c e M o d e l   O T A  
 P I N   - 3 2   - 3 2   N o n e   8  
 P I N A T T R   P i n N a m e   X -  
 P I N A T T R   S p i c e O r d e r   1  
 P I N   - 3 2   - 1 6   N o n e   8  
 P I N A T T R   P i n N a m e   X +  
 P I N A T T R   S p i c e O r d e r   2  
 P I N   - 3 2   1 6   N o n e   8  
 P I N A T T R   P i n N a m e   Y +  
 P I N A T T R   S p i c e O r d e r   3  
 P I N   - 3 2   3 2   N o n e   8  
 P I N A T T R   P i n N a m e   Y -  
 P I N A T T R   S p i c e O r d e r   4  
 P I N   3 2   3 2   N o n e   8  
 P I N A T T R   P i n N a m e   I m i n  
 P I N A T T R   S p i c e O r d e r   6  
 P I N   3 2   0   N o n e   8  
 P I N A T T R   P i n N a m e   I o u t  
 P I N A T T R   S p i c e O r d e r   7  
 P I N   0   4 8   N o n e   8  
 P I N A T T R   P i n N a m e   C O M  
 P I N A T T R   S p i c e O r d e r   8  
 