Version 4
SymbolType CELL X-relay
SYMATTR Description operating/relay device/coil; electromagnetic actuator (solenoid)
LINE Normal -16 32 -16 -32
LINE Normal -16 -32 16 -32
LINE Normal 16 -32 16 32
LINE Normal 16 32 -16 32
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
SYMATTR Prefix X
WINDOW 0 -16 -40 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -40 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
