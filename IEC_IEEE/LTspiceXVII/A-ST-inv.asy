Version 4
SymbolType CELL A-ST-inv
SYMATTR Description Schmitt-trigger inverter; inverter with hysteresis; bi-threshold detector with inverted output
RECTANGLE Normal -32 -32 32 32
CIRCLE Normal 32 -6 44 6
LINE Normal 48 0 44 0
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
LINE Normal -9 8 3 8
LINE Normal 3 8 3 -8
LINE Normal 9 -8 -3 -8
LINE Normal -3 -8 -3 8
SYMATTR SpiceModel Schmitt
PIN -32 0 None 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 48 0 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
