Version 4
SymbolType CELL A-ST-buf
SYMATTR Description Schmitt-trigger buffer; element with hysteresis; bi-threshold detector
RECTANGLE Normal -32 -32 32 32
LINE Normal 48 0 32 0
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
LINE Normal -9 8 3 8
LINE Normal 3 8 3 -8
LINE Normal 9 -8 -3 -8
LINE Normal -3 -8 -3 8
SYMATTR SpiceModel Schmitt
PIN -32 0 None 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 48 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
