Version 4
SymbolType CELL B-source-voltage-0
SYMATTR Description zero voltage/tension source; current meter; ammeter
LINE Normal -32 0 32 0
SYMATTR 162 +
WINDOW 162 -24 16 VCenter 1
SYMATTR Prefix V
WINDOW 0 0 8 VRight 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
