Version 4
SymbolType CELL A-inv
SYMATTR Description negator/inverter/not
RECTANGLE Normal -32 -32 32 32
CIRCLE Normal 32 -6 44 6
LINE Normal 48 0 44 0
WINDOW 3 0 48 Center 2
SYMATTR 164 1
WINDOW 164 0 0 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel OR
PIN -32 0 None 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 48 0 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
