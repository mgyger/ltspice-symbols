Version 4
SymbolType CELL Q-thyristor-tetrode
SYMATTR Description reverse blocking tetrode thyristor, N/P-gate (anode/cathode-side controlled); programmable unijunction transistor (PUT); silicon controlled switch (SCS)
LINE Normal -32 -32 -12 -32
LINE Normal -12 -32 -8 -16
LINE Normal 32 32 12 32
LINE Normal 12 32 8 16
LINE Normal 0 -32 0 32
LINE Normal 0 16 18 -16
LINE Normal 18 -16 -18 -16
LINE Normal -18 -16 0 16
LINE Normal -18 16 18 16
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -32 -32 None 8
PINATTR PinName GA
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName GK
PINATTR SpiceOrder 3
PIN 0 32 None 8
PINATTR PinName K
PINATTR SpiceOrder 4
