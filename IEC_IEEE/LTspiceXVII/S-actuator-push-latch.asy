Version 4
SymbolType GRAPHIC S-actuator-push-latch
SYMATTR Description actuator, operated by pushing (push-button), latching
LINE Normal -10 0 -56 0 Dash
LINE Normal -48 -8 -56 -8
LINE Normal -56 -8 -56 8
LINE Normal -56 8 -48 8
LINE Normal -32 0 -32 -7
LINE Normal -32 -7 -20 0
LINE Normal -20 0 -32 0
