V e r s i o n   4  
 S y m b o l T y p e   C E L L   A - S T  
 S Y M A T T R   D e s c r i p t i o n   S c h m i t t - t r i g g e r   w i t h   c o m p l e m e n t a r y   o u t p u t s ;   e l e m e n t   w i t h   h y s t e r e s i s ;   b i - t h r e s h o l d   d e t e c t o r  
 R E C T A N G L E   N o r m a l   - 3 2   - 3 2   3 2   3 2  
 C I R C L E   N o r m a l   3 2   1 0   4 4   2 2  
 L I N E   N o r m a l   4 8   1 6   4 4   1 6  
 L I N E   N o r m a l   4 8   - 1 6   3 2   - 1 6  
 L I N E   N o r m a l   - 9   8   3   8  
 L I N E   N o r m a l   3   8   3   - 8  
 L I N E   N o r m a l   9   - 8   - 3   - 8  
 L I N E   N o r m a l   - 3   - 8   - 3   8  
 S Y M A T T R   1 6 2   +  
 W I N D O W   1 6 2   - 2 4   - 1 6   C e n t e r   1  
 S Y M A T T R   1 6 3   " 
 W I N D O W   1 6 3   - 2 4   1 6   C e n t e r   1  
 W I N D O W   3   0   4 8   C e n t e r   2  
 S Y M A T T R   P r e f i x   A  
 W I N D O W   0   0   - 4 8   C e n t e r   2  
 S Y M A T T R   S p i c e M o d e l   S c h m i t t  
 P I N   - 3 2   - 1 6   N o n e   8  
 P I N A T T R   P i n N a m e   +  
 P I N A T T R   S p i c e O r d e r   1  
 P I N   - 3 2   1 6   N o n e   8  
 P I N A T T R   P i n N a m e   -  
 P I N A T T R   S p i c e O r d e r   2  
 P I N   4 8   1 6   N o n e   8  
 P I N A T T R   P i n N a m e   _ O U T  
 P I N A T T R   S p i c e O r d e r   6  
 P I N   4 8   - 1 6   N o n e   8  
 P I N A T T R   P i n N a m e   O U T  
 P I N A T T R   S p i c e O r d e r   7  
 P I N   0   3 2   N o n e   8  
 P I N A T T R   P i n N a m e   C O M  
 P I N A T T R   S p i c e O r d e r   8  
 