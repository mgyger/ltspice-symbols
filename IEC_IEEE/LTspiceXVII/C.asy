Version 4
SymbolType CELL C
SYMATTR Description capacitor, non-polarized/polar(ized)/bipolar
LINE Normal -32 0 -3 0
LINE Normal 3 0 32 0
LINE Normal -3 16 -3 -16
LINE Normal 3 16 3 -16
SYMATTR 162 +
WINDOW 162 -16 16 VCenter 1
SYMATTR Prefix C
WINDOW 0 -16 -16 VLeft 2
SYMATTR Value C
WINDOW 3 16 -16 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
