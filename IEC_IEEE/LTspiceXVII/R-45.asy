Version 4
SymbolType CELL R-45
SYMATTR Description resistor (rotated 45�)
LINE Normal 32 32 18 18
LINE Normal -18 -18 -32 -32
LINE Normal -23 -13 -13 -23
LINE Normal -13 -23 23 13
LINE Normal 23 13 13 23
LINE Normal 13 23 -23 -13
SYMATTR 162 +
WINDOW 162 -24 0 Invisible 1
SYMATTR Prefix R
WINDOW 0 -16 16 VRight 2
SYMATTR Value R
WINDOW 3 16 -16 VLeft 2
PIN -32 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
