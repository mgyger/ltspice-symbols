Version 4
SymbolType CELL D-CRD
SYMATTR Description current-regulator/limiting diode (CRD/CLD)
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
CIRCLE Normal -16 16 16 -16
LINE Normal 16 16 16 -16
SYMATTR Prefix X
WINDOW 0 -16 -24 VLeft 2
SYMATTR InstName D
SYMATTR SpiceModel subckt
WINDOW 38 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
