Version 4
SymbolType CELL S-switch-C
SYMATTR Description switch, current controlled
RECTANGLE Normal -32 32 32 -16
LINE Normal 24 0 12 0
LINE Normal 12 0 -17 18
LINE Normal -12 0 -24 0
SYMATTR Prefix W
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value V CSW
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
