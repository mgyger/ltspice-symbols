Version 4
SymbolType CELL B-generator-signal
SYMATTR Description signal/waveform generator (scaled 50%)
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
LINE Normal -16 16 -16 -16
LINE Normal -16 -16 16 -16
LINE Normal 16 -16 16 16
LINE Normal 16 16 -16 16
SYMATTR 162 +
WINDOW 162 -24 24 VCenter 1
SYMATTR 164 G
WINDOW 164 0 0 VCenter 2
SYMATTR Prefix V
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value PWL
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
