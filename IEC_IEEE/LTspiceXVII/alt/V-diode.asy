Version 4
SymbolType CELL V-diode
SYMATTR Description electron(ic)/vacuum tube/valve, diode (scaled) [alternate]
CIRCLE Normal -32 -32 32 32
LINE Normal 0 -16 0 -32
LINE Normal 16 -16 -16 -16
LINE Normal -16 32 -16 16
LINE Normal -16 16 16 16
LINE Normal 16 16 16 23
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN -16 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
