Version 4
SymbolType CELL X-opto-BJT-3
SYMATTR Description phototransistor opto-/photo-coupler, with base terminal [alternate]
RECTANGLE Normal -80 -32 80 32
LINE Normal -48 -32 -48 -16
LINE Normal -48 16 -48 32
LINE Normal -48 16 -30 -16
LINE Normal -30 -16 -66 -16
LINE Normal -66 -16 -48 16
LINE Normal -66 16 -30 16
LINE Normal 16 32 16 0
LINE Normal 16 0 32 0
LINE Normal 64 -32 32 -8
LINE Normal 32 8 64 32
LINE Normal 32 -16 32 16
LINE Normal 40 20 52 23
LINE Normal 52 23 46 12
LINE Normal -19 8 3 8
LINE Normal -19 -8 3 -8
LINE Normal -6 -5 3 -8
LINE Normal 3 -8 -6 -11
LINE Normal -6 11 3 8
LINE Normal 3 8 -6 5
SYMATTR Prefix X
WINDOW 0 88 -16 Left 2
SYMATTR SpiceModel 4N25
WINDOW 38 88 16 Left 2
SYMATTR ModelFile X-opto-BJT-3-LTC.lib
PIN -48 -32 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 32 None 8
PINATTR PinName K
PINATTR SpiceOrder 2
PIN 64 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 3
PIN 64 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 4
PIN 16 32 None 8
PINATTR PinName B
PINATTR SpiceOrder 5
