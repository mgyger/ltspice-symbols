V e r s i o n   4  
 S y m b o l T y p e   C E L L   G - 5 3  
 S Y M A T T R   D e s c r i p t i o n   b i d i r e c t i o n a l / b i l a t e r a l   a n a l o g   s w i t c h / ( d e ) m u l t i p l e x e r   ( c h a n g e - o v e r ,   s i n g l e   p o l e ,   d o u b l e   t h r o w   [ S P D T / S P C O / C ] )   w i t h   e n a b l e   [ a l t e r n a t e ]  
 L I N E   N o r m a l   - 3 2   - 3 2   3 2   - 3 2  
 L I N E   N o r m a l   3 2   - 3 2   3 2   3 2  
 L I N E   N o r m a l   3 2   3 2   - 3 2   3 2  
 L I N E   N o r m a l   - 3 2   3 2   - 3 2   - 3 2  
 L I N E   N o r m a l   2 4   0   1 2   0  
 L I N E   N o r m a l   1 2   0   - 1 7   - 1 1  
 L I N E   N o r m a l   - 1 2   - 6   - 1 2   - 1 6  
 L I N E   N o r m a l   - 1 2   - 1 6   - 2 4   - 1 6  
 L I N E   N o r m a l   - 1 2   6   - 1 2   1 6  
 L I N E   N o r m a l   - 1 2   1 6   - 2 4   1 6  
 L I N E   N o r m a l   - 4 8   - 1 6   - 3 2   - 1 6  
 L I N E   N o r m a l   - 4 8   1 6   - 3 2   1 6  
 L I N E   N o r m a l   6 4   0   3 2   0  
 L I N E   N o r m a l   0   - 4 8   0   - 3 2  
 S Y M A T T R   1 6 2   V +  
 W I N D O W   1 6 2   - 1 6   - 2 4   C e n t e r   0  
 S Y M A T T R   1 6 3   V " 
 W I N D O W   1 6 3   - 1 6   2 4   C e n t e r   0  
 S Y M A T T R   S p i c e M o d e l   s u b c k t  
 W I N D O W   3 8   4 8   2 4   L e f t   2  
 S Y M A T T R   P r e f i x   X  
 W I N D O W   0   4 8   - 2 4   L e f t   2  
 S Y M A T T R   1 6 5   X  
 C I R C L E   N o r m a l   - 6   3 2   6   4 4  
 L I N E   N o r m a l   0   4 8   0   4 4  
 S Y M A T T R   1 6 4   E N  
 W I N D O W   1 6 4   0   2 4   C e n t e r   0  
 P I N   - 4 8   - 1 6   N o n e   8  
 P I N A T T R   P i n N a m e   A  
 P I N A T T R   S p i c e O r d e r   1  
 P I N   - 4 8   1 6   N o n e   8  
 P I N A T T R   P i n N a m e   B  
 P I N A T T R   S p i c e O r d e r   2  
 P I N   6 4   0   N o n e   8  
 P I N A T T R   P i n N a m e   Y  
 P I N A T T R   S p i c e O r d e r   3  
 P I N   0   - 4 8   N o n e   8  
 P I N A T T R   P i n N a m e   X  
 P I N A T T R   S p i c e O r d e r   4  
 P I N   0   4 8   N o n e   8  
 P I N A T T R   P i n N a m e   _ E N  
 P I N A T T R   S p i c e O r d e r   5  
 P I N   - 1 6   - 3 2   N o n e   8  
 P I N A T T R   P i n N a m e   V +  
 P I N A T T R   S p i c e O r d e r   6  
 P I N   - 1 6   3 2   N o n e   8  
 P I N A T T R   P i n N a m e   V -  
 P I N A T T R   S p i c e O r d e r   7  
 