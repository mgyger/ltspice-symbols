Version 4
SymbolType CELL S-DIN-MIDI
SYMATTR Description circular (DIN) connector, 5-pole, IEC 60130-9 IEC-03/04, DIN 41524, for MIDI [alternate]
CIRCLE Normal -80 -32 -16 32
ARC Normal -52 -35 -44 -27 -52 -31 -44 -31
LINE Normal -32 -2 -31 -3
LINE Normal -32 -1 -30 -3
LINE Normal -32 0 -29 -3
LINE Normal -32 1 -28 -3
LINE Normal -32 2 -27 -3
LINE Normal -31 2 -27 -2
LINE Normal -31 3 -26 -2
LINE Normal -30 3 -26 -1
LINE Normal -29 3 -26 0
LINE Normal -28 3 -26 1
LINE Normal -27 3 -26 2
CIRCLE Normal -33 -4 -25 4
LINE Normal -51 17 -50 16
LINE Normal -51 18 -49 16
LINE Normal -51 19 -48 16
LINE Normal -51 20 -47 16
LINE Normal -51 21 -46 16
LINE Normal -50 21 -46 17
LINE Normal -50 22 -45 17
LINE Normal -49 22 -45 18
LINE Normal -48 22 -45 19
LINE Normal -47 22 -45 20
LINE Normal -46 22 -45 21
CIRCLE Normal -52 15 -44 23
LINE Normal -70 -2 -69 -3
LINE Normal -70 -1 -68 -3
LINE Normal -70 0 -67 -3
LINE Normal -70 1 -66 -3
LINE Normal -70 2 -65 -3
LINE Normal -69 2 -65 -2
LINE Normal -69 3 -64 -2
LINE Normal -68 3 -64 -1
LINE Normal -67 3 -64 0
LINE Normal -66 3 -64 1
LINE Normal -65 3 -64 2
CIRCLE Normal -71 -4 -63 4
LINE Normal -38 11 -37 10
LINE Normal -38 12 -36 10
LINE Normal -38 13 -35 10
LINE Normal -38 14 -34 10
LINE Normal -38 15 -33 10
LINE Normal -37 15 -33 11
LINE Normal -37 16 -32 11
LINE Normal -36 16 -32 12
LINE Normal -35 16 -32 13
LINE Normal -34 16 -32 14
LINE Normal -33 16 -32 15
CIRCLE Normal -39 9 -31 17
LINE Normal -64 11 -63 10
LINE Normal -64 12 -62 10
LINE Normal -64 13 -61 10
LINE Normal -64 14 -60 10
LINE Normal -64 15 -59 10
LINE Normal -63 15 -59 11
LINE Normal -63 16 -58 11
LINE Normal -62 16 -58 12
LINE Normal -61 16 -58 13
LINE Normal -60 16 -58 14
LINE Normal -59 16 -58 15
CIRCLE Normal -65 9 -57 17
LINE Normal -48 32 -48 23
CIRCLE Normal 16 -32 80 32
LINE Normal 43 -31 43 -24
LINE Normal 43 -24 53 -24
LINE Normal 53 -24 53 -31
CIRCLE Normal 25 -4 33 4
CIRCLE Normal 44 15 52 23
CIRCLE Normal 63 -4 71 4
CIRCLE Normal 31 9 39 17
CIRCLE Normal 57 9 65 17
LINE Normal 48 32 48 23
SYMATTR Prefix X
WINDOW 0 0 -48 Center 2
SYMATTR InstName S
SYMATTR Value t=-1
SYMATTR ModelFile S-switch.lib
SYMATTR SpiceModel switch.jack4
PIN -32 16 None 8
PINATTR PinName S5
PINATTR SpiceOrder 1
PIN -64 16 None 8
PINATTR PinName S4
PINATTR SpiceOrder 2
PIN -48 32 None 8
PINATTR PinName S2
PINATTR SpiceOrder 3
PIN -48 -32 None 8
PINATTR PinName S0
PINATTR SpiceOrder 4
PIN 32 16 None 8
PINATTR PinName P5
PINATTR SpiceOrder 5
PIN 64 16 None 8
PINATTR PinName P4
PINATTR SpiceOrder 6
PIN 48 32 None 8
PINATTR PinName P2
PINATTR SpiceOrder 7
PIN 48 -32 None 8
PINATTR PinName P0
PINATTR SpiceOrder 8
