Version 4
SymbolType CELL V-pentode-3
SYMATTR Description electron(ic)/vacuum tube/valve, pentode, triode-connected/strapped (scaled) [alternate]
ARC Normal -32 -64 32 0 32 -32 -32 -32
LINE Normal -32 -32 -32 0
ARC Normal -32 -32 32 32 -32 0 32 0
LINE Normal 32 0 32 -32
LINE Normal 0 -48 0 -64
LINE Normal 16 -48 -16 -48
LINE Normal 16 0 -16 0 Dash
LINE Normal 16 -16 -16 -16 Dash
LINE Normal -16 -32 16 -32 Dash
LINE Normal -16 0 -32 0
LINE Normal -16 -16 -48 -16
LINE Normal -48 -16 -48 -64
LINE Normal -48 -64 0 -64
LINE Normal 16 -32 48 -32
LINE Normal 48 -32 48 32
LINE Normal 48 32 16 32
LINE Normal -16 32 -16 16
LINE Normal -16 16 16 16
LINE Normal 16 16 16 32
SYMATTR Prefix X
WINDOW 0 56 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 56 16 Left 2
PIN 0 -64 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN -32 0 None 8
PINATTR PinName G1
PINATTR SpiceOrder 2
PIN -16 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 3
