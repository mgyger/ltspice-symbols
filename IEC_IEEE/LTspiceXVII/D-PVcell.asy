Version 4
SymbolType CELL D-PVcell
SYMATTR Description photovoltaic cell
RECTANGLE Normal -48 -32 48 32
LINE Normal 16 -32 16 -3
LINE Normal 16 3 16 32
LINE Normal -2 -3 34 -3
LINE Normal 6 3 26 3
LINE Normal -35 8 -13 8
LINE Normal -35 -8 -13 -8
LINE Normal -22 -5 -13 -8
LINE Normal -13 -8 -22 -11
LINE Normal -22 11 -13 8
LINE Normal -13 8 -22 5
SYMATTR 162 +
WINDOW 162 0 -16 Center 1
SYMATTR Prefix X
WINDOW 0 56 -16 Left 2
SYMATTR InstName D
SYMATTR SpiceModel subckt
WINDOW 38 56 16 Left 2
PIN 16 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 16 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -48 0 None 8
PINATTR PinName L
PINATTR SpiceOrder 3
