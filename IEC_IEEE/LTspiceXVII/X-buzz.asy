Version 4
SymbolType CELL X-buzz
SYMATTR Description buzzer
ARC Normal 32 -80 -32 -16 -32 -48 32 -48
LINE Normal 32 -48 -32 -48
LINE Normal 32 0 16 0
LINE Normal 16 0 16 -20
LINE Normal -16 -20 -16 0
LINE Normal -16 0 -32 0
SYMATTR Prefix X
WINDOW 0 -16 -56 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -56 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
