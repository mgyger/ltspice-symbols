Version 4
SymbolType CELL D-Schottky-45
SYMATTR Description Schottky diode (rotated 45�)
LINE Normal -32 -32 32 32
LINE Normal 11 11 1 -23
LINE Normal 1 -23 -23 1
LINE Normal -23 1 11 11
SYMATTR Value D
WINDOW 3 16 -16 VLeft 2
LINE Normal 23 9 28 4
LINE Normal 28 4 23 -1
LINE Normal 23 -1 -1 23
LINE Normal -1 23 -6 18
LINE Normal -6 18 -1 13
SYMATTR Prefix D
WINDOW 0 -16 16 VRight 2
PIN -32 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 32 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
