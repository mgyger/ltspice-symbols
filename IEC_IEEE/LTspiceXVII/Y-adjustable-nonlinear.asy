Version 4
SymbolType GRAPHIC Y-adjustable-nonlinear
SYMATTR Description adjustability (non-inherent/extrinsic), non-linear
LINE Normal -16 24 -16 16
LINE Normal -16 16 20 -20
LINE Normal 16 -12 20 -20
LINE Normal 20 -20 12 -16
