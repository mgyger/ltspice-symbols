Version 4
SymbolType CELL S-contact-changeover-MBB
SYMATTR Description change-over contact, make-(before-)break/shorting/bridging; switch, single pole, double throw/change-over (SPDT-MBB/SPCO-MBB/D)
LINE Normal -16 16 -16 32
LINE Normal -16 32 -32 32
LINE Normal 32 0 16 0
LINE Normal 16 0 -23 24
LINE Normal -16 0 -32 0
SYMATTR Prefix X
WINDOW 0 -16 -16 VLeft 2
SYMATTR InstName S
SYMATTR Value t=0
WINDOW 3 16 -16 VLeft 2
SYMATTR ModelFile S-switch.lib
LINE Normal -20 29 -26 19
SYMATTR SpiceModel switch.changeover.mbb
PIN 32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName 4
PINATTR SpiceOrder 3
