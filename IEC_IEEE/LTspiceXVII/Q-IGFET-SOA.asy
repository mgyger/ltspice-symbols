Version 4
SymbolType CELL Q-IGFET-SOA
SYMATTR Description safe operating area (SOA) thermal model, for N-channel insulated-gate field-effect transistor (IGFET/MOSFET)
CIRCLE Normal -24 -35 46 35 Dash
LINE Normal 32 -32 32 -48
LINE Normal -32 16 -48 16
LINE Normal 32 32 32 48
LINE Normal 48 0 46 0 Dash
SYMATTR Prefix X
WINDOW 0 48 -40 Left 2
SYMATTR SpiceModel UserDefined
WINDOW 38 48 40 Left 2
SYMATTR Value2 Tambient=85
SYMATTR ModelFile SOAtherm-NMOS.lib
PIN 32 -48 None 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 48 None 8
PINATTR PinName S
PINATTR SpiceOrder 3
PIN 32 -32 None 8
PINATTR PinName D2
PINATTR SpiceOrder 4
PIN -32 16 None 8
PINATTR PinName G2
PINATTR SpiceOrder 5
PIN 32 32 None 8
PINATTR PinName S2
PINATTR SpiceOrder 6
PIN -32 -32 None 8
PINATTR PinName Tj
PINATTR SpiceOrder 7
PIN 48 0 None 8
PINATTR PinName Tc
PINATTR SpiceOrder 8
