Version 4
SymbolType CELL X-crystal
SYMATTR Description piezoelectric crystal with two electrodes; xtal
LINE Normal -32 0 -12 0
LINE Normal 12 0 32 0
LINE Normal -12 16 -12 -16
LINE Normal 12 16 12 -16
LINE Normal -8 24 -8 -24
LINE Normal -8 -24 8 -24
LINE Normal 8 -24 8 24
LINE Normal 8 24 -8 24
SYMATTR Prefix C
WINDOW 0 -16 -32 VLeft 2
SYMATTR InstName X
SYMATTR Value {1/(2*pi*f*Q*Rs)}
SYMATTR SpiceLine Rser=Rs Lser={Q*Rs/(2*pi*f)} Rpar=1e20 Cpar=Cp
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
