Version 4
SymbolType CELL V-sparkgap-2
SYMATTR Description spark gap, double/symmetric
LINE Normal -32 0 -8 0
LINE Normal 32 0 8 0
LINE Normal -17 3 -8 0
LINE Normal -8 0 -17 -3
LINE Normal 17 3 8 0
LINE Normal 8 0 17 -3
SYMATTR Prefix X
WINDOW 0 -16 -16 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -16 VLeft 2
LINE Normal 0 32 0 -8
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN 0 32 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
