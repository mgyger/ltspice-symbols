Version 4
SymbolType GRAPHIC Y-radiation-coherent
SYMATTR Description radiation, coherent, non-ionizing\n(laser/maser)
LINE Normal 14 -2 -2 14
LINE Normal -14 2 2 -14
LINE Normal -2 -6 2 -14
LINE Normal 2 -14 -6 -10
LINE Normal 10 6 14 -2
LINE Normal 14 -2 6 2
LINE Normal -16 0 0 16
