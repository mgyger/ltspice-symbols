Version 4
SymbolType GRAPHIC E-heat
SYMATTR Description heat source
RECTANGLE Normal -48 -32 48 32
LINE Normal -40 -32 -40 32
LINE Normal 40 32 40 -32
LINE Normal -32 -32 -32 32
LINE Normal 32 32 32 -32
