Version 4
SymbolType CELL B-meter-current
SYMATTR Description current meter/indicator; ammeter
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
CIRCLE Normal -16 16 16 -16
SYMATTR 164 A
WINDOW 164 0 0 VCenter 2
SYMATTR 162 +
WINDOW 162 -24 16 VCenter 1
SYMATTR Prefix V
WINDOW 0 0 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
