Version 4
SymbolType CELL Q-UJT-N
SYMATTR Description unijunction transistor (UJT) with N-type base (PN)
LINE Normal 0 16 32 16
LINE Normal 32 16 32 32
LINE Normal 0 -16 32 -16
LINE Normal 32 -16 32 -32
LINE Normal -32 -16 0 0
LINE Normal 0 -24 0 24
LINE Normal -20 -5 -8 -4
LINE Normal -8 -4 -16 -13
SYMATTR Prefix X
WINDOW 0 48 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 48 16 Left 2
PIN -32 -16 None 8
PINATTR PinName E
PINATTR SpiceOrder 1
PIN 32 32 None 8
PINATTR PinName B1
PINATTR SpiceOrder 2
PIN 32 -32 None 8
PINATTR PinName B2
PINATTR SpiceOrder 3
