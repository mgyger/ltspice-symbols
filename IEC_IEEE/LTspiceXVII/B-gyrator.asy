Version 4
SymbolType CELL B-gyrator
SYMATTR Description gyrator
ARC Normal -12 28 12 4 12 16 -12 16
LINE Normal -32 16 32 16
ARC Normal -12 -4 12 -28 -12 -16 12 -16
LINE Normal -32 -16 32 -16
LINE Normal -16 11 -16 -11
LINE Normal -13 -2 -16 -11
LINE Normal -16 -11 -19 -2
SYMATTR Prefix X
WINDOW 0 -16 -24 VLeft 2
SYMATTR SpiceModel gyrator.G.G
SYMATTR Value R=
SYMATTR Value2 R
WINDOW 123 16 -24 VLeft 2
SYMATTR ModelFile B-gyrator.lib
PIN -32 16 None 8
PINATTR PinName 1+
PINATTR SpiceOrder 1
PIN 32 16 None 8
PINATTR PinName 1-
PINATTR SpiceOrder 2
PIN -32 -16 None 8
PINATTR PinName 2+
PINATTR SpiceOrder 3
PIN 32 -16 None 8
PINATTR PinName 2-
PINATTR SpiceOrder 4
