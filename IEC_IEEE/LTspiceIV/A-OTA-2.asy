Version 4
SymbolType CELL A-OTA-2
SYMATTR Description behavioral operational transconductance amplifier (OTA)
RECTANGLE Normal -32 -32 32 32
SYMATTR 162 -
WINDOW 162 -24 -16 Center 2
SYMATTR 163 +
WINDOW 163 -24 16 Center 2
SYMATTR 166 V/I
WINDOW 166 0 0 Center 2
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel OTA
PIN -32 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 2
PIN 32 0 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
