Version 4
SymbolType CELL Q-BJT-NPN-Darlington
SYMATTR Description NPN bipolar junction transistor (BJT) Darlington pair
LINE Normal 16 16 8 16
LINE Normal 8 16 -8 4
LINE Normal 32 32 16 20
LINE Normal 32 -16 8 -16
LINE Normal 8 -16 -8 -4
LINE Normal 32 -32 32 0
LINE Normal 32 0 16 12
LINE Normal -32 0 -8 0
LINE Normal 16 8 16 24
LINE Normal -8 -8 -8 8
LINE Normal 24 22 28 29
LINE Normal 28 29 20 27
LINE Normal 0 6 4 13
LINE Normal 4 13 -4 11
SYMATTR Prefix X
WINDOW 0 48 -16 Left 2
SYMATTR InstName Q
SYMATTR SpiceModel subckt
WINDOW 38 48 16 Left 2
PIN 32 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -32 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
