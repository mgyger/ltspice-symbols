Version 4
SymbolType CELL D-varicap
SYMATTR Description variable capacitance diode; varactor; varicap
LINE Normal -32 0 32 0
LINE Normal 16 0 -16 -18
LINE Normal -16 -18 -16 18
LINE Normal -16 18 16 0
LINE Normal 16 18 16 -18
LINE Normal 16 -24 3 -24
LINE Normal -3 -24 -16 -24
LINE Normal 3 -32 3 -16
LINE Normal -3 -32 -3 -16
SYMATTR Prefix D
WINDOW 0 -16 -32 VLeft 2
SYMATTR Value D
WINDOW 3 16 -32 VLeft 2
PIN -32 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
