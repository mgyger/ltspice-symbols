Version 4
SymbolType CELL S-switch-V
SYMATTR Description switch, voltage controlled
RECTANGLE Normal -32 32 32 -32
LINE Normal 24 -16 12 -16
LINE Normal 12 -16 -17 2
LINE Normal -12 -16 -24 -16
SYMATTR 163 +
WINDOW 163 -24 16 VCenter 2
SYMATTR 162 -
WINDOW 162 24 16 VCenter 2
SYMATTR Prefix S
WINDOW 0 -16 -40 VLeft 2
SYMATTR Value SW
WINDOW 3 16 -40 VLeft 2
PIN -32 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -32 16 None 8
PINATTR PinName C+
PINATTR SpiceOrder 3
PIN 32 16 None 8
PINATTR PinName C-
PINATTR SpiceOrder 4
