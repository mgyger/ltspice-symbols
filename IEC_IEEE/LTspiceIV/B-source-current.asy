Version 4
SymbolType CELL B-source-current
SYMATTR Description current source\nB: behavioral (I)\nF: current controlled (CCCS)\nI: independent
LINE Normal 0 16 0 -16
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
CIRCLE Normal -16 16 16 -16
SYMATTR 162 +
WINDOW 162 -24 16 VCenter 2
SYMATTR Prefix I
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value I
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
