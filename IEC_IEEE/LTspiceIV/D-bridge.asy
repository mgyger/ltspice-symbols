Version 4
SymbolType CELL D-bridge
SYMATTR Description rectifier in full wave (bridge) connection
LINE Normal 0 -32 32 0
LINE Normal 32 0 0 32
LINE Normal 0 32 -32 0
LINE Normal -32 0 0 -32
LINE Normal 0 16 0 -16
LINE Normal 0 -8 -9 8
LINE Normal -9 8 9 8
LINE Normal 9 8 0 -8
LINE Normal -9 -8 9 -8
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN -32 0 None 8
PINATTR PinName ~1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName ~2
PINATTR SpiceOrder 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 3
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 4
