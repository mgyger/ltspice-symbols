Version 4
SymbolType CELL B-source-voltage
SYMATTR Description voltage/tension source\nB: behavioral (V)\nH: current controlled (CCVS)\nV: independent
LINE Normal -32 0 32 0
CIRCLE Normal -16 16 16 -16
SYMATTR 162 +
WINDOW 162 -24 16 VCenter 2
SYMATTR Prefix V
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value V
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
