Version 4
SymbolType CELL Q-IGBT-N-D
SYMATTR Description N-channel (depletion-type) insulated-gate bipolar transistor (IGBT) with inverse (free-wheeling) diode (simplified)
LINE Normal -32 16 -6 16
LINE Normal -6 16 -6 -16
LINE Normal 32 32 0 8
LINE Normal 32 -32 0 -8
LINE Normal 0 -24 0 24
LINE Normal 8 20 20 23
LINE Normal 20 23 14 12
SYMATTR Prefix Z
WINDOW 0 48 -16 Left 2
SYMATTR Value NIGBT
WINDOW 3 48 16 Left 2
LINE Normal 32 -32 32 32
LINE Normal 32 -8 23 8
LINE Normal 23 8 41 8
LINE Normal 41 8 32 -8
LINE Normal 23 -8 41 -8
PIN 32 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
