Version 4
SymbolType CELL X-opto-BJT-2
SYMATTR Description phototransistor opto-/photo-coupler, without base terminal
RECTANGLE Normal -80 -32 64 32
LINE Normal -48 -32 -48 32
LINE Normal -48 16 -30 -16
LINE Normal -30 -16 -66 -16
LINE Normal -66 -16 -48 16
LINE Normal -66 16 -30 16
LINE Normal 48 -32 16 -8
LINE Normal 16 8 48 32
LINE Normal 16 -16 16 16
LINE Normal 24 20 36 23
LINE Normal 36 23 30 12
LINE Normal -19 8 3 8
LINE Normal -19 -8 3 -8
LINE Normal -6 -5 3 -8
LINE Normal 3 -8 -6 -11
LINE Normal -6 11 3 8
LINE Normal 3 8 -6 5
SYMATTR Prefix X
WINDOW 0 72 -16 Left 2
SYMATTR SpiceModel PC817A
WINDOW 38 72 16 Left 2
SYMATTR ModelFile X-opto-BJT-2-LTC.lib
PIN -48 -32 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 32 None 8
PINATTR PinName K
PINATTR SpiceOrder 2
PIN 48 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 3
PIN 48 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 4
