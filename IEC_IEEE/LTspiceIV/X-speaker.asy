Version 4
SymbolType CELL X-speaker
SYMATTR Description loudspeaker
LINE Normal -16 -8 -16 8
LINE Normal -16 8 16 8
LINE Normal 16 8 16 -8
LINE Normal 16 -8 32 -24
LINE Normal 32 -24 -32 -24
LINE Normal -32 -24 -16 -8
LINE Normal -16 -8 16 -8
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
SYMATTR Prefix X
WINDOW 0 -16 -32 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -32 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
