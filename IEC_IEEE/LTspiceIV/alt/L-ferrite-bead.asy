Version 4
SymbolType CELL L-ferrite-bead
SYMATTR Description ferrite bead/choke (ring) [alternate]
LINE Normal -32 0 -10 0
LINE Normal 10 0 32 0
LINE Normal -1 18 -15 10
LINE Normal -15 10 1 -18
LINE Normal 1 -18 15 -10
LINE Normal 15 -10 -1 18
SYMATTR 162 �
WINDOW 162 -24 8 Invisible 2
SYMATTR Prefix L_Ferrite_Bead
WINDOW 0 24 -16 Left 2
SYMATTR Value L
WINDOW 3 24 16 Left 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
