Version 4
SymbolType CELL G-387
SYMATTR Description 3-input EVEN parity gate [alternate]
ARC Normal -172 -64 -44 64 -49 34 -49 -34
ARC Normal -72 96 56 -32 49 -1 -8 -32
LINE Normal -8 -32 -40 -32
ARC Normal -160 -64 -32 64 -37 34 -37 -34
LINE Normal -40 32 -8 32
ARC Normal -72 -96 56 32 -8 32 49 1
CIRCLE Normal 48 -6 60 6
LINE Normal -48 -16 -34 -16
LINE Normal -48 16 -34 16
LINE Normal 64 0 60 0
LINE Normal -48 0 -32 0
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
PIN -48 -16 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN -48 16 None 8
PINATTR PinName C
PINATTR SpiceOrder 3
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 4
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 5
PIN 64 0 None 8
PINATTR PinName _Y
PINATTR SpiceOrder 6
