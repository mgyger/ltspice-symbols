Version 4
SymbolType CELL X-opto-R
SYMATTR Description photoresistor opto-/photo-coupler [alternate]
RECTANGLE Normal -80 -32 56 32
LINE Normal -48 -32 -48 -16
LINE Normal -48 16 -48 32
LINE Normal -48 16 -30 -16
LINE Normal -30 -16 -66 -16
LINE Normal -66 -16 -48 16
LINE Normal -66 16 -30 16
LINE Normal 32 -32 32 -24
LINE Normal 32 -24 39 -20
LINE Normal 39 -20 25 -12
LINE Normal 25 -12 39 -4
LINE Normal 39 -4 25 4
LINE Normal 25 4 39 12
LINE Normal 39 12 25 20
LINE Normal 25 20 32 24
LINE Normal 32 24 32 32
SYMATTR 162 +
WINDOW 162 16 -16 Invisible 2
LINE Normal -15 8 7 8
LINE Normal -15 -8 7 -8
LINE Normal -2 -5 7 -8
LINE Normal 7 -8 -2 -11
LINE Normal -2 11 7 8
LINE Normal 7 8 -2 5
SYMATTR Prefix X
WINDOW 0 64 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 64 16 Left 2
PIN -48 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN -48 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
PIN 32 -32 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 32 32 None 8
PINATTR PinName 4
PINATTR SpiceOrder 4
