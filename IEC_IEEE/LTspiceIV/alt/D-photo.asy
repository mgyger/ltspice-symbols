Version 4
SymbolType CELL D-photo
SYMATTR Description photodiode [alternate]\nLT1328PD (SFH 205/BPW 34) in LTC.lib\nhttps://tools.analog.com/en/photodiode/
RECTANGLE Normal -32 -32 64 32
LINE Normal 32 32 32 16
LINE Normal 32 -16 32 -32
LINE Normal 32 -16 14 16
LINE Normal 14 16 50 16
LINE Normal 50 16 32 -16
LINE Normal 50 -16 14 -16
LINE Normal -19 8 3 8
LINE Normal -19 -8 3 -8
LINE Normal -6 -5 3 -8
LINE Normal 3 -8 -6 -11
LINE Normal -6 11 3 8
LINE Normal 3 8 -6 5
SYMATTR Prefix X
WINDOW 0 72 -16 Left 2
SYMATTR InstName D
SYMATTR SpiceModel SFH205
WINDOW 38 72 16 Left 2
SYMATTR ModelFile D-photo-LTC.lib
PIN 32 32 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 32 -32 None 8
PINATTR PinName K
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName L
PINATTR SpiceOrder 3
