Version 4
SymbolType CELL L
SYMATTR Description coil/winding/inductor/choke/reactor [alternate]\nwinding with instantaneous voltage polarity indicator/dot
LINE Normal -32 0 -23 0
ARC Normal -23 8 -7 -8 -11 5 -23 0
ARC Normal -13 8 3 -8 -1 5 -9 5
ARC Normal -3 8 13 -8 9 5 1 5
ARC Normal 7 8 23 -8 23 0 11 5
LINE Normal 23 0 32 0
SYMATTR 162 �
WINDOW 162 -23 8 VCenter 2
SYMATTR Prefix L
WINDOW 0 -16 -16 VLeft 2
SYMATTR Value L
WINDOW 3 16 -16 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
