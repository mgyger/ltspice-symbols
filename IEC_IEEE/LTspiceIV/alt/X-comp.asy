Version 4
SymbolType CELL X-comp
SYMATTR Description voltage comparator; operational amplifier (op amp) [alternate]
LINE Normal 32 0 -32 -36
LINE Normal -32 -36 -32 36
LINE Normal -32 36 32 0
LINE Normal 0 -32 0 -18
LINE Normal 0 18 0 32
SYMATTR 162 -
WINDOW 162 -24 16 Center 2
SYMATTR 163 +
WINDOW 163 -24 -16 Center 2
SYMATTR 164 -
WINDOW 164 0 12 Center 2
SYMATTR 165 +
WINDOW 165 0 -12 Center 2
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN -32 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 0 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN 0 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
PIN 32 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 5
