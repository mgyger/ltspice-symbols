Version 4
SymbolType CELL C-45
SYMATTR Description capacitor, non-polarized/polar(ized)/bipolar (rotated 45�) [alternate]
LINE Normal 32 32 2 2
LINE Normal -2 -2 -32 -32
LINE Normal -13 9 9 -13
ARC Normal -11 77 77 -11 15 -7 -7 15
SYMATTR 162 +
WINDOW 162 -24 0 VCenter 2
SYMATTR Prefix C
WINDOW 0 -16 16 VRight 2
SYMATTR Value C
WINDOW 3 16 -16 VLeft 2
PIN -32 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
