Version 4
SymbolType CELL D-TVS
SYMATTR Description breakdown diode, bidirectional; transient voltage suppressor (TVS) [alternate]
LINE Normal 0 0 -32 -18
LINE Normal -32 -18 -32 18
LINE Normal -32 18 0 0
LINE Normal 0 0 32 18
LINE Normal 32 18 32 -18
LINE Normal 32 -18 0 0
LINE Normal -8 18 0 18
LINE Normal 0 18 0 -18
LINE Normal 0 -18 8 -18
SYMATTR Prefix D
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value D
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
