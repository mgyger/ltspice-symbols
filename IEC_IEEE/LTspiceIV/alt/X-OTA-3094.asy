Version 4
SymbolType CELL X-OTA-3094
SYMATTR Description operational transconductance amplifier (OTA) with buffer [alternate]
RECTANGLE Normal -64 -48 80 48
LINE Normal -16 -18 -48 -36
LINE Normal -48 -36 -48 36
LINE Normal -48 36 -16 18
LINE Normal -16 18 -16 -18
LINE Normal -64 -16 -48 -16
LINE Normal -64 16 -48 16
LINE Normal -32 -48 -32 -27
LINE Normal 0 0 0 48
LINE Normal 48 16 32 16
LINE Normal 32 16 16 4
LINE Normal 80 32 64 32
LINE Normal 64 32 48 20
LINE Normal 32 -48 32 -16
LINE Normal 32 -16 16 -4
LINE Normal 64 -48 64 0
LINE Normal 64 0 48 12
LINE Normal -16 0 16 0
LINE Normal 48 8 48 24
LINE Normal 16 -8 16 8
LINE Normal 56 22 60 29
LINE Normal 60 29 52 27
LINE Normal 24 6 28 13
LINE Normal 28 13 20 11
SYMATTR 162 -
WINDOW 162 -40 -16 Center 2
SYMATTR 163 +
WINDOW 163 -40 16 Center 2
SYMATTR 164 -
WINDOW 164 48 40 Center 2
SYMATTR 165 +
WINDOW 165 28 -40 Right 2
SYMATTR Prefix X
WINDOW 0 88 -16 Left 2
SYMATTR SpiceModel CA3094
WINDOW 38 88 16 Left 2
SYMATTR ModelFile X-OTA.lib
PIN 0 48 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 1
PIN -64 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -64 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 3
PIN 48 48 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
PIN -32 -48 None 8
PINATTR PinName Iset
PINATTR SpiceOrder 5
PIN 80 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 6
PIN 32 -48 None 8
PINATTR PinName V+
PINATTR SpiceOrder 7
PIN 64 -48 None 8
PINATTR PinName C
PINATTR SpiceOrder 8
