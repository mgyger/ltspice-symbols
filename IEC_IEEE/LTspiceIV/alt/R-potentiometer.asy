Version 4
SymbolType CELL R-potentiometer
SYMATTR Description potentiometer with movable/rotating/sliding contact [alternate]
LINE Normal 0 -32 0 -20
LINE Normal 0 -20 7 -16
LINE Normal 7 -16 -7 -8
LINE Normal -7 -8 7 0
LINE Normal 7 0 -7 8
LINE Normal -7 8 7 16
LINE Normal 7 16 0 20
LINE Normal 0 20 0 32
LINE Normal 7 0 32 0
LINE Normal 16 -3 7 0
LINE Normal 7 0 16 3
SYMATTR 162 �
WINDOW 162 16 16 Left 2
SYMATTR Prefix X
WINDOW 0 16 -16 Left 2
SYMATTR InstName R
SYMATTR SpiceModel log
WINDOW 38 40 16 Left 2
SYMATTR Value R=
SYMATTR Value2 R
WINDOW 123 -16 -16 Right 2
SYMATTR SpiceLine T=
SYMATTR SpiceLine2 0.5
WINDOW 40 -16 16 Right 2
SYMATTR ModelFile R-potentiometer.lib
PIN 0 32 None 8
PINATTR PinName a
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName b
PINATTR SpiceOrder 2
PIN 0 -32 None 8
PINATTR PinName c
PINATTR SpiceOrder 3
