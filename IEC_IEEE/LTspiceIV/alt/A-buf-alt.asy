Version 4
SymbolType CELL A-buf
SYMATTR Description buffer without specially amplified output [alternate]
LINE Normal 20 0 -32 30
LINE Normal -32 30 -32 -30
LINE Normal -32 -30 20 0
LINE Normal 48 0 20 0
LINE Normal 0 32 0 12
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel OR
PIN -32 0 None 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 48 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
