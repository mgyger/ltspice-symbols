Version 4
SymbolType CELL R-45
SYMATTR Description resistor (rotated 45�) [alternate]
LINE Normal 32 32 18 18
LINE Normal 18 18 10 20
LINE Normal 10 20 14 4
LINE Normal 14 4 -2 8
LINE Normal -2 8 2 -8
LINE Normal 2 -8 -14 -4
LINE Normal -14 -4 -10 -20
LINE Normal -10 -20 -18 -18
LINE Normal -18 -18 -32 -32
SYMATTR 162 +
WINDOW 162 -24 0 Invisible 2
SYMATTR Prefix R
WINDOW 0 -16 16 VRight 2
SYMATTR Value R
WINDOW 3 16 -16 VLeft 2
PIN -32 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
