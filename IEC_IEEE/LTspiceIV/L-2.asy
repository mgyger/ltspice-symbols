Version 4
SymbolType CELL L-2
SYMATTR Description coil; (current/pulse) winding\nwinding with instantaneous voltage polarity indicator/dot
ARC Normal -16 8 0 -8 0 0 -16 0
ARC Normal 0 8 16 -8 16 0 0 0
SYMATTR 162 �
WINDOW 162 -8 8 VCenter 2
SYMATTR Prefix L
WINDOW 0 -16 -16 VLeft 2
SYMATTR Value L
WINDOW 3 16 -16 VLeft 2
PIN -16 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 16 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
