Version 4
SymbolType GRAPHIC S-actuator-push
SYMATTR Description actuator, operated by pushing (push-button), automatic return
LINE Normal -10 0 -56 0 Dash
LINE Normal -48 -8 -56 -8
LINE Normal -56 -8 -56 8
LINE Normal -56 8 -48 8
