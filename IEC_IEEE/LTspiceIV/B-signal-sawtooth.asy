Version 4
SymbolType GRAPHIC B-signal-sawtooth
SYMATTR Description saw-tooth wave (scaled)
LINE Normal -8 12 0 4
LINE Normal 0 4 0 12
LINE Normal 0 12 8 4
LINE Normal 8 4 8 12
