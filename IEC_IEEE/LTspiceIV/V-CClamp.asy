Version 4
SymbolType CELL V-CClamp
SYMATTR Description cold-cathode gas-filled tube/lamp, symmetrical; glow/neon lamp/indicator, AC type (scaled)
CIRCLE Normal -32 32 32 -32
SYMATTR 164 �
WINDOW 164 8 -16 VCenter 2
LINE Normal -32 0 -16 0
CIRCLE Normal -16 4 -8 -4
LINE Normal -16 8 -16 -8
LINE Normal 32 0 16 0
CIRCLE Normal 8 4 16 -4
LINE Normal 16 8 16 -8
SYMATTR Prefix X
WINDOW 0 -16 -40 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -40 VLeft 2
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
