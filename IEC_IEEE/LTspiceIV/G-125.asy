Version 4
SymbolType CELL G-125
SYMATTR Description buffer with 3-state output
RECTANGLE Normal -32 -32 32 32
LINE Normal -48 0 -32 0
LINE Normal 64 0 32 0
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
CIRCLE Normal -6 -44 6 -32
LINE Normal 0 -48 0 -44
LINE Normal 19 8 28 -8
LINE Normal 28 -8 10 -8
LINE Normal 10 -8 19 8
SYMATTR 165 EN
WINDOW 165 0 -24 Center 2
PIN -48 0 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 0 -48 None 8
PINATTR PinName _EN
PINATTR SpiceOrder 2
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 3
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 4
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 5
