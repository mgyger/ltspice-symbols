Version 4
SymbolType CELL G-66
SYMATTR Description bidirectional/bilateral analog switch (make/closing, single pole, single throw, normally open [SPST-NO/A]); transmission gate
RECTANGLE Normal -32 -32 32 32
LINE Normal -24 0 -12 0
LINE Normal -12 0 17 -18
LINE Normal 12 0 24 0
LINE Normal -48 0 -32 0
LINE Normal 64 0 32 0
LINE Normal 0 -48 0 -32
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
SYMATTR 165 X
WINDOW 165 0 -24 Center 2
PIN -48 0 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 2
PIN 0 -48 None 8
PINATTR PinName X
PINATTR SpiceOrder 3
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 4
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 5
