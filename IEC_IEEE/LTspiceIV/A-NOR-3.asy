Version 4
SymbolType CELL A-NOR-3
SYMATTR Description OR with negated output (NOR)
RECTANGLE Normal -32 -32 32 32
CIRCLE Normal 32 -6 44 6
LINE Normal 64 0 44 0
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR 164 1
WINDOW 164 0 0 Center 2
SYMATTR SpiceModel OR
PIN -48 -16 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 64 0 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
