Version 4
SymbolType CELL X-bell
SYMATTR Description acoustic signalling device; (single-stroke) bell; horn/whistle
ARC Normal -32 16 32 -48 32 -16 -32 -16
LINE Normal -32 -16 32 -16
LINE Normal 32 0 16 0
LINE Normal 16 0 16 -16
LINE Normal -16 -16 -16 0
LINE Normal -16 0 -32 0
SYMATTR Prefix X
WINDOW 0 -16 -56 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -56 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
