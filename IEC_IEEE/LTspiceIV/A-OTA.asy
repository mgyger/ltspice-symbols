Version 4
SymbolType CELL A-OTA
SYMATTR Description multiplier with complementary inputs and operational transconductance amplifier (OTA)
RECTANGLE Normal -32 -48 32 48
LINE Normal -16 -32 -16 -16
LINE Normal -16 16 -16 32
SYMATTR 162 -
WINDOW 162 -24 -32 Center 2
SYMATTR 163 +
WINDOW 163 -24 -16 Center 2
SYMATTR 164 +
WINDOW 164 -24 16 Center 2
SYMATTR 165 -
WINDOW 165 -24 32 Center 2
SYMATTR 166 �/I
WINDOW 166 0 0 Center 2
WINDOW 3 0 64 Center 2
SYMATTR Prefix A
WINDOW 0 0 -64 Center 2
SYMATTR SpiceModel OTA
PIN -32 -32 None 8
PINATTR PinName X-
PINATTR SpiceOrder 1
PIN -32 -16 None 8
PINATTR PinName X+
PINATTR SpiceOrder 2
PIN -32 16 None 8
PINATTR PinName Y+
PINATTR SpiceOrder 3
PIN -32 32 None 8
PINATTR PinName Y-
PINATTR SpiceOrder 4
PIN 32 32 None 8
PINATTR PinName Imin
PINATTR SpiceOrder 6
PIN 32 0 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 7
PIN 0 48 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
