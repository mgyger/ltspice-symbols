Version 4
SymbolType CELL X-earphone
SYMATTR Description earphone
LINE Normal -16 -16 -16 8
LINE Normal -16 8 16 8
LINE Normal 16 8 16 -16
LINE Normal -24 -16 24 -16
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
SYMATTR Prefix X
WINDOW 0 -16 -24 VLeft 2
SYMATTR SpiceModel subckt
WINDOW 38 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
