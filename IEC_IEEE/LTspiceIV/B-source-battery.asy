Version 4
SymbolType CELL B-source-battery
SYMATTR Description primary cell; secondary/rechargeable cell; battery of primary or secondary cells; DC supply
LINE Normal -32 0 -3 0
LINE Normal 3 0 32 0
LINE Normal -3 32 -3 -32
LINE Normal 3 16 3 -16
SYMATTR 162 +
WINDOW 162 -16 16 VCenter 2
SYMATTR Prefix V
WINDOW 0 -16 -16 VLeft 2
SYMATTR Value V
WINDOW 3 16 -16 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
