Version 4
SymbolType CELL D-diac
SYMATTR Description bidirectional diode thyristor; diac
LINE Normal 32 0 16 0
LINE Normal -16 0 -32 0
LINE Normal 16 -2 -16 16
LINE Normal -16 16 16 34
LINE Normal 16 34 16 -34
LINE Normal -16 2 16 -16
LINE Normal 16 -16 -16 -34
LINE Normal -16 -34 -16 34
LINE Normal 0 -34 0 34
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN -32 0 None 8
PINATTR PinName T1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName T2
PINATTR SpiceOrder 2
