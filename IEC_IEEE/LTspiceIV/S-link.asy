Version 4
SymbolType CELL S-link
SYMATTR Description connecting link, closed
LINE Normal -32 0 -24 0
LINE Normal 24 0 32 0
LINE Normal -20 4 20 4
LINE Normal 20 -4 -20 -4
CIRCLE Normal -24 4 -16 -4
CIRCLE Normal 16 4 24 -4
SYMATTR Prefix R
WINDOW 0 0 -16 VLeft 2
SYMATTR InstName S
SYMATTR Value 0
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
