Version 4
SymbolType CELL B-source-current-V
SYMATTR Description current source, voltage controlled (VCCS)
LINE Normal 0 16 0 -16
LINE Normal -32 0 -16 0
LINE Normal 16 0 32 0
CIRCLE Normal -16 16 16 -16
SYMATTR 162 +
WINDOW 162 -24 16 VCenter 2
SYMATTR 163 +
WINDOW 163 -24 48 VCenter 2
SYMATTR Prefix G
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value G
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -32 32 None 8
PINATTR PinName C+
PINATTR SpiceOrder 3
PIN 32 32 None 8
PINATTR PinName C-
PINATTR SpiceOrder 4
