Version 4
SymbolType CELL A-XOR
SYMATTR Description exclusive-OR with complementary outputs (XOR/XNOR)
RECTANGLE Normal -32 -48 32 48
CIRCLE Normal 32 10 44 22
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
LINE Normal 64 16 44 16
LINE Normal 64 -16 32 -16
WINDOW 3 0 64 Center 2
SYMATTR Prefix A
WINDOW 0 0 -64 Center 2
SYMATTR 164 =1
WINDOW 164 0 0 Center 2
SYMATTR SpiceModel XOR
PIN -32 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -48 -16 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN -48 16 None 8
PINATTR PinName 4
PINATTR SpiceOrder 4
PIN -32 32 None 8
PINATTR PinName 5
PINATTR SpiceOrder 5
PIN 64 16 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 64 -16 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 48 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
