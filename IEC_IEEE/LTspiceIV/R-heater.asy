Version 4
SymbolType CELL R-heater
SYMATTR Description heating element/resistor
LINE Normal -12 7 -12 -7
LINE Normal 0 7 0 -7
LINE Normal 12 7 12 -7
LINE Normal -32 0 -24 0
LINE Normal 24 0 32 0
LINE Normal -24 7 -24 -7
LINE Normal -24 -7 24 -7
LINE Normal 24 -7 24 7
LINE Normal 24 7 -24 7
SYMATTR 162 +
WINDOW 162 -16 16 Invisible 2
SYMATTR Prefix R
WINDOW 0 -16 -16 VLeft 2
SYMATTR Value R
WINDOW 3 16 -16 VLeft 2
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
