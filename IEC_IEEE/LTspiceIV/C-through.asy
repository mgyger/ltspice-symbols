Version 4
SymbolType CELL C-through
SYMATTR Description capacitor, lead/feed-through
LINE Normal -32 0 32 0
LINE Normal 0 6 0 32
LINE Normal -16 -6 16 -6
LINE Normal -16 6 16 6
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR InstName C
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN 32 0 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
