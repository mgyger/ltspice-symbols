Version 4
SymbolType CELL L
SYMATTR Description coil/winding/inductor/choke/reactor\nwinding with instantaneous voltage polarity indicator/dot
ARC Normal -32 8 -16 -8 -16 0 -32 0
ARC Normal -16 8 0 -8 0 0 -16 0
ARC Normal 0 8 16 -8 16 0 0 0
ARC Normal 16 8 32 -8 32 0 16 0
SYMATTR 162 �
WINDOW 162 -24 8 VCenter 2
SYMATTR Prefix L
WINDOW 0 -16 -16 VLeft 2
SYMATTR Value L
WINDOW 3 16 -16 VLeft 2
PIN -32 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
