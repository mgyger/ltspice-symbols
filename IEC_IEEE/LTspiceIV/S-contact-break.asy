Version 4
SymbolType CELL S-contact-break
SYMATTR Description break/opening contact; switch, single pole, single throw, normally closed (SPST-NC/B)
LINE Normal 32 0 16 0
LINE Normal 16 0 -23 -15
LINE Normal -16 -16 -16 0
LINE Normal -16 0 -32 0
SYMATTR Prefix X
WINDOW 0 -16 -32 VLeft 2
SYMATTR InstName S
SYMATTR SpiceModel switch.break
SYMATTR Value t=0
WINDOW 3 16 -32 VLeft 2
SYMATTR ModelFile S-switch.lib
PIN 32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
