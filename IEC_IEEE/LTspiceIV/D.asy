Version 4
SymbolType CELL D
SYMATTR Description semiconductor diode
LINE Normal -32 0 32 0
LINE Normal 16 0 -16 -18
LINE Normal -16 -18 -16 18
LINE Normal -16 18 16 0
LINE Normal 16 18 16 -18
SYMATTR Prefix D
WINDOW 0 -16 -24 VLeft 2
SYMATTR Value D
WINDOW 3 16 -24 VLeft 2
PIN -32 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
