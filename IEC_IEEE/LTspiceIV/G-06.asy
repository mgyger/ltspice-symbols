Version 4
SymbolType CELL G-06
SYMATTR Description negator/inverter/not, open-drain/collector
RECTANGLE Normal -32 -32 32 32
CIRCLE Normal 32 -6 44 6
LINE Normal -48 0 -32 0
LINE Normal 64 0 44 0
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
LINE Normal 20 8 28 0
LINE Normal 28 0 20 -8
LINE Normal 20 -8 12 0
LINE Normal 12 0 20 8
LINE Normal 12 8 28 8
PIN -48 0 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 2
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 3
PIN 64 0 None 8
PINATTR PinName _Y
PINATTR SpiceOrder 4
