Version 4
SymbolType CELL X-OTA
SYMATTR Description operational transconductance amplifier (OTA)
RECTANGLE Normal -32 -32 32 32
SYMATTR 162 -
WINDOW 162 -24 -16 Center 2
SYMATTR 163 +
WINDOW 163 -24 16 Center 2
SYMATTR 164 -
WINDOW 164 0 24 Center 2
SYMATTR 165 +
WINDOW 165 0 -24 Center 2
SYMATTR 166 V/I
WINDOW 166 0 0 Center 2
SYMATTR Prefix �
WINDOW 0 40 -16 Left 2
WINDOW 3 40 16 Left 2
PIN 32 0 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 1
PIN 0 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 2
PIN 0 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 3
PIN -32 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 4
PIN -32 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 5
