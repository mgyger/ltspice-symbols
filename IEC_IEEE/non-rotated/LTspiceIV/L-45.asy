Version 4
SymbolType CELL L-45
SYMATTR Description coil/winding/inductor/choke/reactor (rotated 45�)\nwinding with instantaneous voltage polarity indicator/dot (rotated 45�)
LINE Normal -32 32 -24 24
LINE Normal 24 -24 32 -32
ARC Normal -26 10 -10 26 -26 26 -10 10
ARC Normal -14 -2 2 14 -14 14 2 -2
ARC Normal -2 -14 14 2 -2 2 14 -14
ARC Normal 10 -26 26 -10 10 -10 26 -26
SYMATTR 162 �
WINDOW 162 8 -24 Center 2
SYMATTR Prefix L
WINDOW 0 -16 -16 Right 2
SYMATTR Value L
WINDOW 3 16 16 Left 2
PIN 32 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
