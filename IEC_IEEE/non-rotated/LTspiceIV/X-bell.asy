Version 4
SymbolType CELL X-bell
SYMATTR Description acoustic signalling device; (single-stroke) bell; horn/whistle
ARC Normal -16 -32 48 32 16 32 16 -32
LINE Normal 16 -32 16 32
LINE Normal 0 32 0 16
LINE Normal 0 16 16 16
LINE Normal 16 -16 0 -16
LINE Normal 0 -16 0 -32
SYMATTR Prefix X
WINDOW 0 56 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 56 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
