Version 4
SymbolType CELL B-meter-current
SYMATTR Description current meter/indicator; ammeter
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
CIRCLE Normal -16 -16 16 16
SYMATTR 164 A
WINDOW 164 0 0 Center 2
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR Prefix V
WINDOW 0 24 0 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
