Version 4
SymbolType CELL S-contact-make
SYMATTR Description make/closing contact; switch, single pole, single throw, normally open (SPST-NO/A)
LINE Normal 0 32 0 16
LINE Normal 0 16 -24 -23
LINE Normal 0 -16 0 -32
SYMATTR Prefix X
WINDOW 0 16 -16 Left 2
SYMATTR InstName S
SYMATTR SpiceModel switch.make
SYMATTR Value t=0
WINDOW 3 16 16 Left 2
SYMATTR ModelFile S-switch.lib
PIN 0 32 None 8
PINATTR PinName 3
PINATTR SpiceOrder 1
PIN 0 -32 None 8
PINATTR PinName 4
PINATTR SpiceOrder 2
