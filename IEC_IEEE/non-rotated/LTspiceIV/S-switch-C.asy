Version 4
SymbolType CELL S-switch-C
SYMATTR Description switch, current controlled
RECTANGLE Normal -32 -32 16 32
LINE Normal 0 24 0 12
LINE Normal 0 12 -18 -17
LINE Normal 0 -12 0 -24
SYMATTR Prefix W
WINDOW 0 24 -16 Left 2
SYMATTR Value V CSW
WINDOW 3 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
