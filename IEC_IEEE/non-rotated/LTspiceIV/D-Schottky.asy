Version 4
SymbolType CELL D-Schottky
SYMATTR Description Schottky diode
LINE Normal 0 -32 0 32
LINE Normal 0 16 18 -16
LINE Normal 18 -16 -18 -16
LINE Normal -18 -16 0 16
LINE Normal -10 8 -18 8
LINE Normal -18 8 -18 16
LINE Normal -18 16 18 16
LINE Normal 18 16 18 24
LINE Normal 18 24 10 24
SYMATTR Prefix D
WINDOW 0 24 -16 Left 2
SYMATTR Value D
WINDOW 3 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
