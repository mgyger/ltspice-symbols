Version 4
SymbolType GRAPHIC Y-adjustable-preset
SYMATTR Description adjustability (non-inherent/extrinsic), pre-set (trimmer)
LINE Normal -16 16 16 -16
LINE Normal 13 -19 19 -13
