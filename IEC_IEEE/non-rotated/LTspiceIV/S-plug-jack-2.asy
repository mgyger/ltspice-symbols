Version 4
SymbolType CELL S-plug-jack-2
SYMATTR Description (telephone type) plug and jack, 2-pole
LINE Normal -64 16 -48 16
LINE Normal -64 0 -48 0
LINE Normal -48 -2 -47 -3
LINE Normal -48 -1 -46 -3
LINE Normal -48 0 -45 -3
LINE Normal -48 1 -44 -3
LINE Normal -48 2 -43 -3
LINE Normal -48 3 -42 -3
LINE Normal -47 3 -41 -3
LINE Normal -46 3 -40 -3
LINE Normal -45 3 -39 -3
LINE Normal -44 3 -38 -3
LINE Normal -43 3 -37 -3
LINE Normal -42 3 -36 -3
LINE Normal -41 3 -35 -3
LINE Normal -40 3 -34 -3
LINE Normal -39 3 -33 -3
LINE Normal -38 3 -32 -3
LINE Normal -37 3 -31 -3
LINE Normal -36 3 -30 -3
LINE Normal -35 3 -29 -3
LINE Normal -34 3 -28 -3
LINE Normal -33 3 -27 -3
LINE Normal -32 3 -26 -3
LINE Normal -31 3 -25 -3
LINE Normal -30 3 -24 -3
LINE Normal -29 3 -23 -3
LINE Normal -28 3 -22 -3
LINE Normal -27 3 -21 -3
LINE Normal -26 3 -20 -3
LINE Normal -25 3 -19 -3
LINE Normal -24 3 -18 -3
LINE Normal -23 3 -17 -3
LINE Normal -22 3 -16 -3
LINE Normal -21 3 -16 -2
LINE Normal -20 3 -16 -1
LINE Normal -19 3 -16 0
LINE Normal -18 3 -16 1
LINE Normal -17 3 -16 2
LINE Normal -48 -3 -16 -3
LINE Normal -16 -3 -16 3
LINE Normal -16 3 -48 3
LINE Normal -48 3 -48 -3
LINE Normal -48 14 -47 13
LINE Normal -48 15 -46 13
LINE Normal -48 16 -45 13
LINE Normal -48 17 -44 13
LINE Normal -48 18 -43 13
LINE Normal -48 19 -42 13
LINE Normal -47 19 -41 13
LINE Normal -46 19 -40 13
LINE Normal -45 19 -39 13
LINE Normal -44 19 -38 13
LINE Normal -43 19 -37 13
LINE Normal -42 19 -36 13
LINE Normal -41 19 -35 13
LINE Normal -40 19 -34 13
LINE Normal -39 19 -33 13
LINE Normal -38 19 -32 13
LINE Normal -37 19 -32 14
LINE Normal -36 19 -32 15
LINE Normal -35 19 -32 16
LINE Normal -34 19 -32 17
LINE Normal -33 19 -32 18
LINE Normal -48 13 -32 13
LINE Normal -32 13 -32 19
LINE Normal -32 19 -48 19
LINE Normal -48 19 -48 13
LINE Normal 64 -16 32 -16
LINE Normal 32 -16 24 -2
LINE Normal 24 -2 16 -16
LINE Normal -8 -15 -7 -16
LINE Normal -8 -14 -6 -16
LINE Normal -8 -13 -5 -16
LINE Normal -8 -12 -4 -16
LINE Normal -8 -11 -3 -16
LINE Normal -8 -10 -2 -16
LINE Normal -8 -9 -1 -16
LINE Normal -8 -8 0 -16
LINE Normal -7 -8 0 -15
LINE Normal -6 -8 0 -14
LINE Normal -5 -8 0 -13
LINE Normal -4 -8 0 -12
LINE Normal -3 -8 0 -11
LINE Normal -2 -8 0 -10
LINE Normal -1 -8 0 -9
LINE Normal -8 13 -7 12
LINE Normal -8 14 -6 12
LINE Normal -8 15 -5 12
LINE Normal -8 16 -4 12
LINE Normal -8 17 -3 12
LINE Normal -8 18 -2 12
LINE Normal -8 19 -1 12
LINE Normal -8 20 0 12
LINE Normal -7 20 0 13
LINE Normal -6 20 0 14
LINE Normal -5 20 0 15
LINE Normal -4 20 0 16
LINE Normal -3 20 0 17
LINE Normal -2 20 0 18
LINE Normal -1 20 0 19
LINE Normal -8 -8 0 -8
LINE Normal -8 12 0 12
LINE Normal 64 16 0 16
LINE Normal 0 20 0 -16
LINE Normal 0 -16 -8 -16
LINE Normal -8 -16 -8 20
LINE Normal -8 20 0 20
SYMATTR Prefix X
WINDOW 0 0 -32 Center 2
SYMATTR InstName S
SYMATTR Value t=-1
SYMATTR ModelFile S-switch.lib
SYMATTR SpiceModel switch.jack2
PIN -64 0 None 8
PINATTR PinName T1
PINATTR SpiceOrder 1
PIN -64 16 None 8
PINATTR PinName S1
PINATTR SpiceOrder 2
PIN 64 -16 None 8
PINATTR PinName T2
PINATTR SpiceOrder 3
PIN 64 16 None 8
PINATTR PinName S2
PINATTR SpiceOrder 4
