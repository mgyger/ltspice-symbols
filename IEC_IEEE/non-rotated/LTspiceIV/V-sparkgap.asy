Version 4
SymbolType CELL V-sparkgap
SYMATTR Description spark gap
LINE Normal 0 -32 0 -8
LINE Normal 0 32 0 8
LINE Normal -3 -17 0 -8
LINE Normal 0 -8 3 -17
LINE Normal -3 17 0 8
LINE Normal 0 8 3 17
SYMATTR Prefix X
WINDOW 0 16 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 16 16 Left 2
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
