Version 4
SymbolType CELL G-332
SYMATTR Description 3-input OR gate
RECTANGLE Normal -32 -32 32 32
LINE Normal 64 0 32 0
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
LINE Normal -48 0 -32 0
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR 164 1
WINDOW 164 0 0 Center 2
PIN -48 -16 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN -48 16 None 8
PINATTR PinName C
PINATTR SpiceOrder 3
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 4
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 5
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 6
