Version 4
SymbolType CELL X-crystal
SYMATTR Description piezoelectric crystal with two electrodes; xtal
LINE Normal 0 -32 0 -12
LINE Normal 0 12 0 32
LINE Normal -16 -12 16 -12
LINE Normal -16 12 16 12
LINE Normal -24 -8 24 -8
LINE Normal 24 -8 24 8
LINE Normal 24 8 -24 8
LINE Normal -24 8 -24 -8
SYMATTR Prefix C
WINDOW 0 32 -16 Left 2
SYMATTR InstName X
SYMATTR Value {1/(2*pi*f*Q*Rs)}
SYMATTR SpiceLine Rser=Rs Lser={Q*Rs/(2*pi*f)} Rpar=1e20 Cpar=Cp
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
