Version 4
SymbolType CELL B-source-voltage
SYMATTR Description voltage/tension source\nB: behavioral (V)\nH: current controlled (CCVS)\nV: independent
LINE Normal 0 -32 0 32
CIRCLE Normal -16 -16 16 16
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR Prefix V
WINDOW 0 24 -16 Left 2
SYMATTR Value V
WINDOW 3 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
