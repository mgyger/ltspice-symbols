********************************************************************************
* http://www.analog.com/Analog_Root/static/techSupport/designTools/spiceModels/license/spice_general.html
********************************************************************************
*
* LICENSE AGREEMENT
* 
* IMPORTANT NOTICE. Read this Agreement carefully before downloading or using this spice model. BY DOWNLOADING OR USING THIS SPICE MODEL IN ANY WAY YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTAND AND AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU DO NOT AGREE TO THESE TERMS OR IF YOU DO NOT HAVE THE AUTHORITY DESCRIBED BELOW, DO NOT DOWNLOAD THIS SPICE MODEL, DO NOT USE THIS SPICE MODEL IN ANY WAY, AND PROMPTLY DELETE OR DESTROY ANY COPIES OF THIS SPICE MODEL IN YOUR POSSESSION
* 
* If You are entering into this Agreement on behalf of a company or other organization, You represent that You have the authority to bind it to this Agreement and commit funds on its behalf, and the terms "You" and "Your" will refer to that company or organization.
* 
* Analog Devices SPICE Model Library
* 
* Copyright 1995-2015 by Analog Devices, Inc.
* 
* The information in this SPICE Model is protected under United States copyright laws. ADI hereby grants You a nonexclusive, nontransferable license to use this SPICE Model as long as You abide by the terms of this Agreement.
* 
* You may not sell, loan, rent, lease, or license the SPICE Model, in whole, in part, or in modified form, to anyone outside Your company. You may modify this SPICE Model to suit Your specific applications, and You may make copies of this SPICE Model for use within Your company.
* 
* You may include copies of Analog Devices' SPICE models with any software you sell or distribute. However, you may not make changes to the redistributed copies of Analog Devices SPICE models other than to:
* 
*     1. Include comments.
*     2. Change nomenclature so that it will run on Your company's software. No changes may be made that affect the performance or function of the model.
* 
* Analog Devices includes SPICE models in its library that have been developed by third parties. These models may not be redistributed.
* 
* This SPICE Model is provided AS IS, WHERE IS, AND WITH NO WARRANTY OF ANY KIND EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* 
* ADI'S LIABILITY FOR DAMAGES TO YOU FOR ANY CAUSE WHATSOEVER, REGARDLESS OF THE FORM OF ANY CLAIM OR ACTION, SHALL NOT EXCEED ONE HUNDRED US DOLLARS ($100.00 US). IN NO EVENT SHALL ADI BE LIABLE FOR ANY LOSS OF DATA, PROFITS OR USE OF THE SPICE MODEL, OR FOR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL OR OTHER INDIRECT DAMAGES ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE SPICE MODEL.
* 
* This Agreement shall be governed by and construed in accordance with the laws of the Commonwealth of Massachusetts, without regard to its choice of law provisions. The parties agree that the United Nations Convention on Contracts for the International Sale of Goods is hereby excluded in its entirety from this Agreement. The sole jurisdiction and venue for all actions related to the subject matter hereof shall be the state and federal courts located in Suffolk County, Massachusetts.

********************************************************************************
* http://www.analog.com/media/en/simulation-models/spice-models/mat02.cir
********************************************************************************
*
* MAT02 SPICE Macro-model                 
* Description: Amplifier
* Generic Desc: Low Noise, Matched Dual Transistor
* Developed by: DFB / PMI
* Revision History: 08/10/2012 - Updated to new header style
* 1.0 (04/1990)
* Copyright 1990, 2012 by Analog Devices, Inc.
*
* Refer to http://www.analog.com/Analog_Root/static/techSupport/designTools/spiceModels/license/spice_general.html for License Statement. Use of this model 
* indicates your acceptance of the terms and provisions in the License Statement.
*
* BEGIN Notes:
*
* Not Modeled:
*    
* Parameters modeled include: 
*
* END Notes
*
* Node assignments
*              C1
*              | B1 
*              | | E1
*              | | | E2
*              | | | | B2
*              | | | | | C2
*              | | | | | |
.SUBCKT MAT02  1 2 3 5 6 7
Q1   1  2  3   NMAT
Q2   7  6  5   NMAT
D1   3  2      DMAT1
D2   5  6      DMAT1
D3   4  3      DMAT1
D4   4  5      DMAT1
D5   4  1      DMAT2
D6   4  7      DMAT2
.MODEL    DMAT1  D(IS=2E-16 RS=20)
.MODEL    DMAT2  D(IS=1E-14 VJ=0.6 CJO=40E-12)
.MODEL    NMAT NPN(BF=500 IS=6E-13 VAF=150 BR=0.5 VAR=7
+ RB=13 RC=10 RE=0.3 CJE=82E-12 VJE=0.7 MJE=0.4 TF=0.3E-9 
+ TR=5E-9 CJC=33E-12 VJC=0.55 MJC=0.5 CJS=0 IKF=0.300
+ PTF=25)
.ENDS

********************************************************************************
* http://www.analog.com/media/en/simulation-models/spice-models/MAT12.cir
********************************************************************************
*
* MAT12 SPICE Macro-model    
* Audio Dual Matched NPN Transistor
* 8/2015, Rev. A                                    
* Copyright 2010 by Analog Devices, Inc.
*
*
* Node assignments
*              C1
*              | B1 
*              | | E1
*              | | | E2
*              | | | | B2
*              | | | | | C2
*              | | | | | |
.SUBCKT MAT12  1 2 3 5 6 7
Q1   1  2  3   NMAT
Q2   7  6  5   NMAT
D1   3  2      DMAT1
D2   5  6      DMAT1
D3   4  3      DMAT1
D4   4  5      DMAT1
D5   4  1      DMAT2
D6   4  7      DMAT2
.MODEL    DMAT1  D(IS=2E-16 RS=20)
.MODEL    DMAT2  D(IS=1E-14 VJ=0.6 CJO=40E-12)
.MODEL    NMAT NPN(BF=500 IS=6E-13 VAF=150 BR=0.5 VAR=7
+ RB=13 RC=10 RE=0.3 CJE=82E-12 VJE=0.7 MJE=0.4 TF=0.3E-9 
+ TR=5E-9 CJC=33E-12 VJC=0.55 MJC=0.5 CJS=0 IKF=0.300
+ PTF=25)
.ENDS

********************************************************************************
* http://www.analog.com/media/en/simulation-models/spice-models/SSM2212.cir
********************************************************************************
*
* SSM2212 SPICE Macro-model                 3/2012, Rev. A
*                                      
*
* Copyright 2010 by Analog Devices, Inc.
*
*
* Node assignments
*              C1
*              | B1 
*              | | E1
*              | | | E2
*              | | | | B2
*              | | | | | C2
*              | | | | | |
.SUBCKT SSM2212  1 2 3 5 6 7
Q1   1  2  3   NMAT
Q2   7  6  5   NMAT
D1   3  2      DMAT1
D2   5  6      DMAT1
D3   4  3      DMAT1
D4   4  5      DMAT1
D5   4  1      DMAT2
D6   4  7      DMAT2
.MODEL    DMAT1  D(IS=2E-16 RS=20)
.MODEL    DMAT2  D(IS=1E-14 VJ=0.6 CJO=40E-12)
.MODEL    NMAT NPN(BF=500 IS=6E-13 VAF=150 BR=0.5 VAR=7
+ RB=13 RC=10 RE=0.3 CJE=82E-12 VJE=0.7 MJE=0.4 TF=0.3E-9 
+ TR=5E-9 CJC=33E-12 VJC=0.55 MJC=0.5 CJS=0 IKF=0.300
+ PTF=25)
.ENDS

