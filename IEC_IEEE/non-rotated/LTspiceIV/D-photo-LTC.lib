* Copyright � Linear Technology Corp. 2000-2012.  All rights reserved.
*
* Linear Technology Corporation hereby grants the users of this
* macromodel a non-exclusive, nontransferrable license to use this
* macromodel under the following conditions:
*
* The user agrees that this macromodel is licensed from Linear
* Technology and agrees that the macromodel may be used, loaned,
* given away or included in other model libraries as long as this
* notice and the model in its entirety and unchanged is included.
* No right to make derivative works or modifications to the
* macromodel is granted hereby. All such rights are reserved.
*
* This model is provided as is. Linear Technology makes no
* warranty, either expressed or implied about the suitability or
* fitness of this model for any particular purpose. In no event
* will Linear Technology be liable for special, collateral,
* incidental or consequential damages in connection with or arising
* out of the use of this macromodel. It should be remembered that
* models are a simplification of the actual circuit.
*
* Linear Technology reserves the right to change these macromodels
* without prior notice. Contact Linear Technology at 1630 McCarthy
* Blvd., Milpitas, CA, 95035-7417 or telephone (408) 432-1900 for
* datasheets on the actual amplifiers or the latest macromodels.
* Also check our website at:
*
* www.linear.com
*
* LT and LTC are registered trademarks of Linear Technology Corporation
*
* This library of macromodels is being supplied to LTC users as an
* aid to circuit design. While the models reflect reasonably
* close similarity to corresponding devices in performance terms,
* their use is not suggested as a replacement for breadboarding.
* Simulation should be used as a forerunner or a supplement to
* traditional lab testing.
*
* Users should very carefully note the following factors regarding
* these models: Model performance in general will reflect typical
* baseline specs for a given device, and certain aspects of per-
* formance may not be modeled fully. While reasonable care has
* been taken in their preparation, we cannot be responsible for
* correct application on any and all computer systems. Model users
* are hereby notified that these models are supplied "as is", with
* no direct or implied responsibility on the part of LTC for their
* operation within a customer circuit or system. Further, Linear
* Technology Corporation reserves the right to change these models
* without prior notice.
*
* In all cases, the current data sheet information for a given real
* device is your final design guideline, and is the only actual
* performance guarantee. For further technical information, refer
* to individual device data sheets.
*
*
*HERE IS A PHOTODIODE FOR TESTING THE LT1328, SIMILAR TO SFH-205.
*A=ANODE (COMMON), K=CATHODE (OUTPUT), AND L=LIGHT INPUT.
*DRIVE LIGHT INPUT (L) WITH A CURRENT SOURCE CONNECTED AS FOLLOWS:
*IPD A L; USE PULSE, PWL, ETC. WAVEFORMS. THE 1328PD TAKES CARE OF
*FREQUENCY RESPONSE, SETTLING, AND DIODE CAPACITANCE.
.SUBCKT SFH205 A K L
;.SUBCKT LT1328PD A K L
DPD A K DPD
;DPD 0 K DPD
R1 L A 1
C1 L A 18N ;SETTLING TAIL FOR 125NS PULSES
GD K A L A 1.0
ET 2 A L A 0.18
R3 2 3 1
C2 3 A 1U
GT K A 3 A 1
*
.Model DPD D(IS=8.5E-10 XTI=0.5 N=1.35 IKF=390u TIKF=0
+ RS=150 TRS1=0.022 TRS2=5u  CJO=72p VJ=0.4 M=0.5
+ FC=0.5 TT=10p EG=0.69 ISR=100p NR=2 BV=100 IBV=100u
+ NBV=1 IBVL=0 NBVL=1 KF=0 AF=1)
.ENDS SFH205
;.ENDS LT1328PD
*

