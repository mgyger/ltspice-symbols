Version 4
SymbolType CELL A-ST
SYMATTR Description Schmitt-trigger with complementary outputs; element with hysteresis; bi-threshold detector
RECTANGLE Normal -32 -32 32 32
CIRCLE Normal 32 10 44 22
LINE Normal 48 16 44 16
LINE Normal 48 -16 32 -16
LINE Normal -9 8 3 8
LINE Normal 3 8 3 -8
LINE Normal 9 -8 -3 -8
LINE Normal -3 -8 -3 8
SYMATTR 162 +
WINDOW 162 -24 -16 Center 2
SYMATTR 163 -
WINDOW 163 -24 16 Center 2
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel Schmitt
PIN -32 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 48 16 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 48 -16 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
