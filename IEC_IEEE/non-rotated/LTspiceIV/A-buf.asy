Version 4
SymbolType CELL A-buf
SYMATTR Description buffer without specially amplified output
RECTANGLE Normal -32 -32 32 32
LINE Normal 48 0 32 0
WINDOW 3 0 48 Center 2
SYMATTR 164 1
WINDOW 164 0 0 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel OR
PIN -32 0 None 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 48 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
