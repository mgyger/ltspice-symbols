Version 4
SymbolType CELL X-opamp
SYMATTR Description operational amplifier (op amp); voltage comparator
RECTANGLE Normal -32 -32 32 32
LINE Normal 0 0 -16 -9
LINE Normal -16 -9 -16 9
LINE Normal -16 9 0 0
CIRCLE Normal 4 -3 10 3
CIRCLE Normal 10 -3 16 3
SYMATTR 162 -
WINDOW 162 -24 -16 Center 2
SYMATTR 163 +
WINDOW 163 -24 16 Center 2
SYMATTR 164 -
WINDOW 164 0 24 Center 2
SYMATTR 165 +
WINDOW 165 0 -24 Center 2
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN -32 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 0 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN 0 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
PIN 32 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 5
