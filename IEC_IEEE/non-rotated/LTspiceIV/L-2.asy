Version 4
SymbolType CELL L-2
SYMATTR Description coil; (current/pulse) winding\nwinding with instantaneous voltage polarity indicator/dot
ARC Normal -8 -16 8 0 0 0 0 -16
ARC Normal -8 0 8 16 0 16 0 0
SYMATTR 162 �
WINDOW 162 -8 -8 Center 2
SYMATTR Prefix L
WINDOW 0 16 -16 Left 2
SYMATTR Value L
WINDOW 3 16 16 Left 2
PIN 0 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
