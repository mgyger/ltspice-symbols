Version 4
SymbolType CELL S-plug-jack-3-break
SYMATTR Description (telephone type) plug and jack, 3-pole, with break/shunt contacts
LINE Normal -64 16 -48 16
LINE Normal -64 0 -48 0
LINE Normal -64 -16 -48 -16
LINE Normal -48 -18 -47 -19
LINE Normal -48 -17 -46 -19
LINE Normal -48 -16 -45 -19
LINE Normal -48 -15 -44 -19
LINE Normal -48 -14 -43 -19
LINE Normal -48 -13 -42 -19
LINE Normal -47 -13 -41 -19
LINE Normal -46 -13 -40 -19
LINE Normal -45 -13 -39 -19
LINE Normal -44 -13 -38 -19
LINE Normal -43 -13 -37 -19
LINE Normal -42 -13 -36 -19
LINE Normal -41 -13 -35 -19
LINE Normal -40 -13 -34 -19
LINE Normal -39 -13 -33 -19
LINE Normal -38 -13 -32 -19
LINE Normal -37 -13 -31 -19
LINE Normal -36 -13 -30 -19
LINE Normal -35 -13 -29 -19
LINE Normal -34 -13 -28 -19
LINE Normal -33 -13 -27 -19
LINE Normal -32 -13 -26 -19
LINE Normal -31 -13 -25 -19
LINE Normal -30 -13 -24 -19
LINE Normal -29 -13 -23 -19
LINE Normal -28 -13 -22 -19
LINE Normal -27 -13 -21 -19
LINE Normal -26 -13 -20 -19
LINE Normal -25 -13 -19 -19
LINE Normal -24 -13 -18 -19
LINE Normal -23 -13 -17 -19
LINE Normal -22 -13 -16 -19
LINE Normal -21 -13 -16 -18
LINE Normal -20 -13 -16 -17
LINE Normal -19 -13 -16 -16
LINE Normal -18 -13 -16 -15
LINE Normal -17 -13 -16 -14
LINE Normal -48 -19 -16 -19
LINE Normal -16 -19 -16 -13
LINE Normal -16 -13 -48 -13
LINE Normal -48 -13 -48 -19
LINE Normal -48 -2 -47 -3
LINE Normal -48 -1 -46 -3
LINE Normal -48 0 -45 -3
LINE Normal -48 1 -44 -3
LINE Normal -48 2 -43 -3
LINE Normal -48 3 -42 -3
LINE Normal -47 3 -41 -3
LINE Normal -46 3 -40 -3
LINE Normal -45 3 -39 -3
LINE Normal -44 3 -38 -3
LINE Normal -43 3 -37 -3
LINE Normal -42 3 -36 -3
LINE Normal -41 3 -35 -3
LINE Normal -40 3 -34 -3
LINE Normal -39 3 -33 -3
LINE Normal -38 3 -32 -3
LINE Normal -37 3 -31 -3
LINE Normal -36 3 -30 -3
LINE Normal -35 3 -29 -3
LINE Normal -34 3 -28 -3
LINE Normal -33 3 -27 -3
LINE Normal -32 3 -26 -3
LINE Normal -31 3 -25 -3
LINE Normal -30 3 -24 -3
LINE Normal -29 3 -24 -2
LINE Normal -28 3 -24 -1
LINE Normal -27 3 -24 0
LINE Normal -26 3 -24 1
LINE Normal -25 3 -24 2
LINE Normal -48 -3 -24 -3
LINE Normal -24 -3 -24 3
LINE Normal -24 3 -48 3
LINE Normal -48 3 -48 -3
LINE Normal -48 14 -47 13
LINE Normal -48 15 -46 13
LINE Normal -48 16 -45 13
LINE Normal -48 17 -44 13
LINE Normal -48 18 -43 13
LINE Normal -48 19 -42 13
LINE Normal -47 19 -41 13
LINE Normal -46 19 -40 13
LINE Normal -45 19 -39 13
LINE Normal -44 19 -38 13
LINE Normal -43 19 -37 13
LINE Normal -42 19 -36 13
LINE Normal -41 19 -35 13
LINE Normal -40 19 -34 13
LINE Normal -39 19 -33 13
LINE Normal -38 19 -32 13
LINE Normal -37 19 -32 14
LINE Normal -36 19 -32 15
LINE Normal -35 19 -32 16
LINE Normal -34 19 -32 17
LINE Normal -33 19 -32 18
LINE Normal -48 13 -32 13
LINE Normal -32 13 -32 19
LINE Normal -32 19 -48 19
LINE Normal -48 19 -48 13
LINE Normal 64 -32 32 -32
LINE Normal 32 -32 24 -18
LINE Normal 24 -18 16 -32
LINE Normal 64 16 24 16
LINE Normal 24 16 16 2
LINE Normal 16 2 8 16
LINE Normal -8 -31 -7 -32
LINE Normal -8 -30 -6 -32
LINE Normal -8 -29 -5 -32
LINE Normal -8 -28 -4 -32
LINE Normal -8 -27 -3 -32
LINE Normal -8 -26 -2 -32
LINE Normal -8 -25 -1 -32
LINE Normal -8 -24 0 -32
LINE Normal -7 -24 0 -31
LINE Normal -6 -24 0 -30
LINE Normal -5 -24 0 -29
LINE Normal -4 -24 0 -28
LINE Normal -3 -24 0 -27
LINE Normal -2 -24 0 -26
LINE Normal -1 -24 0 -25
LINE Normal -8 13 -7 12
LINE Normal -8 14 -6 12
LINE Normal -8 15 -5 12
LINE Normal -8 16 -4 12
LINE Normal -8 17 -3 12
LINE Normal -8 18 -2 12
LINE Normal -8 19 -1 12
LINE Normal -8 20 0 12
LINE Normal -7 20 0 13
LINE Normal -6 20 0 14
LINE Normal -5 20 0 15
LINE Normal -4 20 0 16
LINE Normal -3 20 0 17
LINE Normal -2 20 0 18
LINE Normal -1 20 0 19
LINE Normal -8 -24 0 -24
LINE Normal -8 12 0 12
LINE Normal 64 32 0 32
LINE Normal 0 32 0 20
LINE Normal 0 20 0 -32
LINE Normal 0 -32 -8 -32
LINE Normal -8 -32 -8 20
LINE Normal -8 20 0 20
SYMATTR Prefix X
WINDOW 0 0 -48 Center 2
SYMATTR InstName S
SYMATTR Value t=-1
SYMATTR ModelFile S-switch.lib
LINE Normal 64 -16 48 -16
LINE Normal 48 -16 48 -36
LINE Normal 64 0 48 0
LINE Normal 48 0 48 20
SYMATTR SpiceModel switch.jack3break
PIN -64 -16 None 8
PINATTR PinName T1
PINATTR SpiceOrder 1
PIN -64 0 None 8
PINATTR PinName R1
PINATTR SpiceOrder 2
PIN -64 16 None 8
PINATTR PinName S1
PINATTR SpiceOrder 3
PIN 64 -32 None 8
PINATTR PinName T2
PINATTR SpiceOrder 4
PIN 64 16 None 8
PINATTR PinName R2
PINATTR SpiceOrder 5
PIN 64 32 None 8
PINATTR PinName S2
PINATTR SpiceOrder 6
PIN 64 -16 None 8
PINATTR PinName T3
PINATTR SpiceOrder 7
PIN 64 0 None 8
PINATTR PinName R3
PINATTR SpiceOrder 8
