Version 4
SymbolType GRAPHIC E-IGFET-D
SYMATTR Description envelope, for insulated-gate field-effect transistor (IGFET) with inverse (body-drain) diode
CIRCLE Normal -24 -35 46 35
