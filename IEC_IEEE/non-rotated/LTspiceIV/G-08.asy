Version 4
SymbolType CELL G-08
SYMATTR Description 2-input AND gate
RECTANGLE Normal -32 -32 32 32
LINE Normal 64 0 32 0
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR 164 &
WINDOW 164 0 0 Center 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
PIN -48 -16 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 3
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 4
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 5
