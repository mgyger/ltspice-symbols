Version 4
SymbolType CELL G-17
SYMATTR Description Schmitt-trigger buffer; element with hysteresis; bi-threshold detector
RECTANGLE Normal -32 -32 32 32
LINE Normal -48 0 -32 0
LINE Normal 64 0 32 0
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
LINE Normal -29 8 -17 8
LINE Normal -17 8 -17 -8
LINE Normal -11 -8 -23 -8
LINE Normal -23 -8 -23 8
PIN -48 0 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 2
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
