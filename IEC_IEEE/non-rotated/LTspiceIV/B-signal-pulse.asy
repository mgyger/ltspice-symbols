Version 4
SymbolType GRAPHIC B-signal-pulse
SYMATTR Description pulse, positive-going (scaled)
LINE Normal -4 12 -2 12
LINE Normal -2 12 -2 4
LINE Normal -2 4 2 4
LINE Normal 2 4 2 12
LINE Normal 2 12 4 12
