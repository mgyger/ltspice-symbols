Version 4
SymbolType GRAPHIC Y-adjustable
SYMATTR Description adjustability (non-inherent/extrinsic)
LINE Normal -16 16 20 -20
LINE Normal 16 -12 20 -20
LINE Normal 20 -20 12 -16
