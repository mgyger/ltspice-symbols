Version 4
SymbolType CELL S-switch-V
SYMATTR Description switch, voltage controlled
RECTANGLE Normal -32 -32 32 32
LINE Normal 16 24 16 12
LINE Normal 16 12 -2 -17
LINE Normal 16 -12 16 -24
SYMATTR 163 +
WINDOW 163 -16 -24 Center 2
SYMATTR 162 -
WINDOW 162 -16 24 Center 2
SYMATTR Prefix S
WINDOW 0 40 -16 Left 2
SYMATTR Value SW
WINDOW 3 40 16 Left 2
PIN 16 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 16 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -16 -32 None 8
PINATTR PinName C+
PINATTR SpiceOrder 3
PIN -16 32 None 8
PINATTR PinName C-
PINATTR SpiceOrder 4
