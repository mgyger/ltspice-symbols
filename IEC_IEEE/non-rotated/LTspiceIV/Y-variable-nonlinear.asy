Version 4
SymbolType GRAPHIC Y-variable-nonlinear
SYMATTR Description variability (inherent/intrinsic), non-linear\n- -t�/t�: temperature dependent; negative/positive temperature coefficient (NTC/PTC)\n- U: voltage dependent
LINE Normal -16 24 -16 16
LINE Normal -16 16 16 -16
SYMATTR 164 t�
WINDOW 164 -24 16 Right 2
