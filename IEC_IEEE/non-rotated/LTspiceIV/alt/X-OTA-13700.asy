Version 4
SymbolType CELL X-OTA-13700
SYMATTR Description dual operational transconductance amplifier (OTA) with linearizing diodes and audio buffer [alternate]
RECTANGLE Normal -64 -48 80 48
LINE Normal -48 12 -48 36
LINE Normal -48 36 -16 18
LINE Normal -16 18 -16 -18
LINE Normal -16 -18 -48 -36
LINE Normal -48 -36 -48 -12
LINE Normal -48 -4 -48 4
LINE Normal -64 0 -48 0
LINE Normal -64 -16 -48 -16
LINE Normal -64 16 -48 16
LINE Normal -32 -48 -32 -27
LINE Normal -16 0 0 0
LINE Normal 0 0 0 48
LINE Normal 48 16 40 16
LINE Normal 40 16 24 4
LINE Normal 80 32 64 32
LINE Normal 64 32 48 20
LINE Normal 64 -16 40 -16
LINE Normal 40 -16 24 -4
LINE Normal 64 -48 64 0
LINE Normal 64 0 48 12
LINE Normal 16 48 16 0
LINE Normal 16 0 24 0
LINE Normal 48 8 48 24
LINE Normal 24 -8 24 8
LINE Normal 56 22 60 29
LINE Normal 60 29 52 27
LINE Normal 32 6 36 13
LINE Normal 36 13 28 11
LINE Normal -48 -12 -53 -4
LINE Normal -53 -4 -43 -4
LINE Normal -43 -4 -48 -12
LINE Normal -53 -12 -43 -12
LINE Normal -48 12 -43 4
LINE Normal -43 4 -53 4
LINE Normal -53 4 -48 12
LINE Normal -53 12 -43 12
SYMATTR 162 -
WINDOW 162 -38 -16 Center 2
SYMATTR 163 +
WINDOW 163 -38 16 Center 2
SYMATTR 164 -
WINDOW 164 48 40 Center 2
SYMATTR 165 +
WINDOW 165 60 -40 Right 2
SYMATTR Prefix X
WINDOW 0 88 -16 Left 2
SYMATTR SpiceModel LM13700
WINDOW 38 88 16 Left 2
SYMATTR ModelFile X-OTA.lib
PIN -32 -48 None 8
PINATTR PinName Iset
PINATTR SpiceOrder 1
PIN -64 0 None 8
PINATTR PinName Id
PINATTR SpiceOrder 2
PIN -64 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 3
PIN -64 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 4
PIN 0 48 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 5
PIN 48 48 None 8
PINATTR PinName V-
PINATTR SpiceOrder 6
PIN 16 48 None 8
PINATTR PinName B
PINATTR SpiceOrder 7
PIN 80 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 8
PIN 64 -48 None 8
PINATTR PinName V+
PINATTR SpiceOrder 9
