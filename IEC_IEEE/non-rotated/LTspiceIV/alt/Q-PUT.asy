Version 4
SymbolType CELL Q-PUT
SYMATTR Description reverse blocking triode thyristor, N-gate (anode-side controlled); programmable unijunction transistor (PUT) [alternate]
LINE Normal -32 -32 -18 -32
LINE Normal -18 -32 -9 -16
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
LINE Normal 0 16 18 -16
LINE Normal 18 -16 -18 -16
LINE Normal -18 -16 0 16
LINE Normal -18 16 18 16
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -32 -32 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 0 32 None 8
PINATTR PinName K
PINATTR SpiceOrder 3
