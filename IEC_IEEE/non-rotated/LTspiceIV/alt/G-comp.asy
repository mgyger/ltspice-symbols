Version 4
SymbolType CELL G-comp
SYMATTR Description voltage comparator; operational amplifier (op amp) [alternate]
LINE Normal 32 0 -32 -36
LINE Normal -32 -36 -32 36
LINE Normal -32 36 32 0
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
LINE Normal 64 0 32 0
LINE Normal -16 -32 -16 -27
LINE Normal -16 32 -16 27
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR 162 +
WINDOW 162 -8 -16 Center 2
SYMATTR 163 -
WINDOW 163 -8 16 Center 2
SYMATTR 164 +
WINDOW 164 -24 -16 Center 2
SYMATTR 165 -
WINDOW 165 -24 16 Center 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
PIN -48 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
PIN 64 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 5
