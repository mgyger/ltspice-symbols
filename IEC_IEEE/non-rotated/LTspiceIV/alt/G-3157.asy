Version 4
SymbolType CELL G-3157
SYMATTR Description bidirectional/bilateral analog switch/(de)multiplexer (change-over, single pole, double throw, break-[before-]make/non-shorting [SPDT-BBM/SPCO-BBM/C]) [alternate]
LINE Normal -32 -32 32 -32
LINE Normal 32 -32 32 32
LINE Normal 32 32 -32 32
LINE Normal -32 32 -32 -32
LINE Normal 24 0 12 0
LINE Normal 12 0 -17 -11
LINE Normal -12 -6 -12 -16
LINE Normal -12 -16 -24 -16
LINE Normal -12 6 -12 16
LINE Normal -12 16 -24 16
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
LINE Normal 64 0 32 0
LINE Normal 0 -48 0 -32
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
SYMATTR 165 X
PIN -48 -16 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 3
PIN 0 -48 None 8
PINATTR PinName X
PINATTR SpiceOrder 4
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 5
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 6
