Version 4
SymbolType CELL C-45
SYMATTR Description capacitor, non-polarized/polar(ized)/bipolar (rotated 45�) [alternate]
LINE Normal -32 32 -2 2
LINE Normal 2 -2 32 -32
LINE Normal -9 -13 13 9
ARC Normal -77 -11 11 77 7 15 -15 -7
SYMATTR 162 +
WINDOW 162 0 -24 Center 2
SYMATTR Prefix C
WINDOW 0 -16 -16 Right 2
SYMATTR Value C
WINDOW 3 16 16 Left 2
PIN 32 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
