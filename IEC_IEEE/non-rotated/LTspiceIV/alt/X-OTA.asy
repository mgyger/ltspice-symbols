Version 4
SymbolType CELL X-OTA
SYMATTR Description operational transconductance amplifier (OTA) [alternate]
LINE Normal 16 -18 -16 -36
LINE Normal -16 -36 -16 36
LINE Normal -16 36 16 18
LINE Normal 16 18 16 -18
LINE Normal -32 -16 -16 -16
LINE Normal -32 16 -16 16
LINE Normal 32 0 16 0
LINE Normal 0 32 0 27
LINE Normal 0 -32 0 -27
SYMATTR 162 -
WINDOW 162 -8 -16 Center 2
SYMATTR 163 +
WINDOW 163 -8 16 Center 2
SYMATTR 164 -
WINDOW 164 8 16 Center 2
SYMATTR 165 +
WINDOW 165 8 -16 Center 2
WINDOW 166 0 0 Center 2
SYMATTR Prefix �
WINDOW 0 40 -16 Left 2
WINDOW 3 40 16 Left 2
PIN 32 0 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 1
PIN 0 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 2
PIN 0 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 3
PIN -32 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 4
PIN -32 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 5
