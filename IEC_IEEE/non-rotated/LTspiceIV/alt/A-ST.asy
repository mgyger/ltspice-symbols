Version 4
SymbolType CELL A-ST
SYMATTR Description Schmitt-trigger with complementary outputs; element with hysteresis; bi-threshold detector [alternate]
LINE Normal 32 0 -32 -36
LINE Normal -32 -36 -32 36
LINE Normal -32 36 32 0
CIRCLE Normal 10 10 22 22
LINE Normal -17 8 -5 8
LINE Normal -5 8 -5 -8
LINE Normal 1 -8 -11 -8
LINE Normal -11 -8 -11 8
LINE Normal 48 -16 4 -16
LINE Normal 48 16 22 16
LINE Normal 0 32 0 18
SYMATTR 162 +
WINDOW 162 -24 -16 Center 2
SYMATTR 163 -
WINDOW 163 -24 16 Center 2
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel Schmitt
PIN -32 -16 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 48 16 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 48 -16 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
