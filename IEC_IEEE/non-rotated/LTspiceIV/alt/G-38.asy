Version 4
SymbolType CELL G-38
SYMATTR Description 2-input AND gate with negated output (NAND), open-drain/collector [alternate]
LINE Normal -32 32 16 32
ARC Normal -16 -32 48 32 16 32 16 -32
LINE Normal 16 -32 -32 -32
LINE Normal -32 -32 -32 32
CIRCLE Normal 48 -6 60 6
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
LINE Normal 64 0 60 0
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR 163 -
WINDOW 163 -16 24 Center 2
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
LINE Normal 35 8 43 0
LINE Normal 43 0 35 -8
LINE Normal 35 -8 27 0
LINE Normal 27 0 35 8
LINE Normal 27 8 43 8
PIN -48 -16 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
PIN 64 0 None 8
PINATTR PinName _Y
PINATTR SpiceOrder 5
