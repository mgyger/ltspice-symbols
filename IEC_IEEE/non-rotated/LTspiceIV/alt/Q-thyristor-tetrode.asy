Version 4
SymbolType CELL Q-thyristor-tetrode
SYMATTR Description reverse blocking tetrode thyristor, N/P-gate (anode/cathode-side controlled); programmable unijunction transistor (PUT); silicon controlled switch (SCS) [alternate]
LINE Normal -32 -32 -18 -32
LINE Normal -18 -32 -9 -16
LINE Normal 32 32 18 32
LINE Normal 18 32 9 16
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
LINE Normal 0 16 18 -16
LINE Normal 18 -16 -18 -16
LINE Normal -18 -16 0 16
LINE Normal -18 16 18 16
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -32 -32 None 8
PINATTR PinName GA
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName GK
PINATTR SpiceOrder 3
PIN 0 32 None 8
PINATTR PinName K
PINATTR SpiceOrder 4
