Version 4
SymbolType CELL D-CRD
SYMATTR Description current-regulator/limiting diode (CRD/CLD) [alternate]
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
LINE Normal 0 16 18 -16
LINE Normal 18 -16 -18 -16
LINE Normal -18 -16 0 16
LINE Normal -18 16 18 16
LINE Normal 18 12 18 20
LINE Normal -18 12 -18 20
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR InstName D
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
