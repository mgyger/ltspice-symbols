Version 4
SymbolType CELL L-45
SYMATTR Description coil/winding/inductor/choke/reactor (rotated 45�) [alternate]\nwinding with instantaneous voltage polarity indicator/dot (rotated 45�)
LINE Normal -32 32 -18 18
LINE Normal 18 -18 32 -32
ARC Normal -20 4 -4 20 -20 20 -12 4
ARC Normal -12 -4 4 12 -12 4 -4 -4
ARC Normal -4 -12 12 4 -4 -4 4 -12
ARC Normal 4 -20 20 -4 4 -12 20 -20
SYMATTR 162 �
WINDOW 162 9 -25 Center 2
SYMATTR Prefix L
WINDOW 0 -16 -16 Right 2
SYMATTR Value L
WINDOW 3 16 16 Left 2
PIN 32 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
