Version 4
SymbolType CELL A-OTA
SYMATTR Description multiplier with complementary inputs and operational transconductance amplifier (OTA) [alternate]
LINE Normal 21 -24 -21 -48
LINE Normal -21 -48 -21 48
LINE Normal -21 48 21 24
LINE Normal 21 24 21 -24
LINE Normal -32 -32 -21 -32
LINE Normal -32 -16 -21 -16
LINE Normal -32 16 -21 16
LINE Normal -32 32 -21 32
LINE Normal 32 0 21 0
LINE Normal 32 32 7 32
LINE Normal 0 48 0 36
SYMATTR 162 -
WINDOW 162 -13 -32 Center 2
SYMATTR 163 +
WINDOW 163 -13 -16 Center 2
SYMATTR 164 +
WINDOW 164 -13 16 Center 2
SYMATTR 165 -
WINDOW 165 -13 32 Center 2
SYMATTR 166 �
WINDOW 166 0 0 Center 2
WINDOW 3 0 64 Center 2
SYMATTR Prefix A
WINDOW 0 0 -64 Center 2
SYMATTR SpiceModel OTA
PIN -32 -32 None 8
PINATTR PinName X-
PINATTR SpiceOrder 1
PIN -32 -16 None 8
PINATTR PinName X+
PINATTR SpiceOrder 2
PIN -32 16 None 8
PINATTR PinName Y+
PINATTR SpiceOrder 3
PIN -32 32 None 8
PINATTR PinName Y-
PINATTR SpiceOrder 4
PIN 32 32 None 8
PINATTR PinName Imin
PINATTR SpiceOrder 6
PIN 32 0 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 7
PIN 0 48 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
