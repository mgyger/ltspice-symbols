Version 4
SymbolType CELL Q-triac
SYMATTR Description bidirectional triode thyristor; triac [alternate]
LINE Normal -32 32 -25 32
LINE Normal -25 32 -16 16
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
LINE Normal 2 -16 -16 16
LINE Normal -16 16 -34 -16
LINE Normal -34 -16 34 -16
LINE Normal -2 16 16 -16
LINE Normal 16 -16 34 16
LINE Normal 34 16 -34 16
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN 0 -32 None 8
PINATTR PinName MT2
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 0 32 None 8
PINATTR PinName MT1
PINATTR SpiceOrder 3
