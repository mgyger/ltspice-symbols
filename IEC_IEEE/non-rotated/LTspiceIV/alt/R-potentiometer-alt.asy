Version 4
SymbolType CELL R-potentiometer
SYMATTR Description potentiometer with movable/rotating/sliding contact [alternate]
LINE Normal 0 -32 0 -24
LINE Normal 0 -24 7 -20
LINE Normal 7 -20 -7 -12
LINE Normal -7 -12 7 -4
LINE Normal 7 -4 -7 4
LINE Normal -7 4 7 12
LINE Normal 7 12 -7 20
LINE Normal -7 20 0 24
LINE Normal 0 24 0 32
LINE Normal 7 -4 32 -4
LINE Normal 32 -4 32 0
LINE Normal 16 -7 7 -4
LINE Normal 7 -4 16 -1
SYMATTR 162 �
WINDOW 162 16 16 Left 2
SYMATTR Prefix X
WINDOW 0 16 -16 Left 2
SYMATTR InstName R
SYMATTR SpiceModel log
WINDOW 38 40 16 Left 2
SYMATTR Value R=
SYMATTR Value2 R
WINDOW 123 -16 -16 Right 2
SYMATTR SpiceLine T=
SYMATTR SpiceLine2 0.5
WINDOW 40 -16 16 Right 2
SYMATTR ModelFile R-potentiometer.lib
PIN 0 32 None 8
PINATTR PinName a
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName b
PINATTR SpiceOrder 2
PIN 0 -32 None 8
PINATTR PinName c
PINATTR SpiceOrder 3
