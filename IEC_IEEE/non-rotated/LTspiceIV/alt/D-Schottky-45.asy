Version 4
SymbolType CELL D-Schottky-45
SYMATTR Description Schottky diode (rotated 45�) [alternate]
LINE Normal 32 -32 11 -11
LINE Normal -11 11 -32 32
LINE Normal -11 11 23 1
LINE Normal 23 1 -1 -23
LINE Normal -1 -23 -11 11
SYMATTR Value D
WINDOW 3 16 16 Left 2
LINE Normal -9 23 -4 28
LINE Normal -4 28 1 23
LINE Normal 1 23 -23 -1
LINE Normal -23 -1 -18 -6
LINE Normal -18 -6 -13 -1
SYMATTR Prefix D
WINDOW 0 -16 -16 Right 2
PIN 32 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
