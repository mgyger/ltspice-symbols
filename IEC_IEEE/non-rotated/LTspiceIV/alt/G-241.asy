Version 4
SymbolType CELL G-241
SYMATTR Description negator/inverter/not with 3-state output [alternate]
LINE Normal 32 0 -32 -36
LINE Normal -32 -36 -32 36
LINE Normal -32 36 32 0
CIRCLE Normal 32 -6 44 6
LINE Normal -48 0 -32 0
LINE Normal 64 0 44 0
LINE Normal -16 -32 -16 -27
LINE Normal -16 32 -16 27
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR 162 +
WINDOW 162 -16 -20 Center 2
SYMATTR 163 -
WINDOW 163 -16 20 Center 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
LINE Normal 0 -48 0 -18
PIN -48 0 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 0 -48 None 8
PINATTR PinName EN
PINATTR SpiceOrder 2
PIN 64 0 None 8
PINATTR PinName _Y
PINATTR SpiceOrder 3
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 4
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 5
