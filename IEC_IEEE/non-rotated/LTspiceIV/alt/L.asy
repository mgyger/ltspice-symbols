Version 4
SymbolType CELL L
SYMATTR Description coil/winding/inductor/choke/reactor [alternate]\nwinding with instantaneous voltage polarity indicator/dot
LINE Normal 0 -32 0 -23
ARC Normal -8 -23 8 -7 -5 -11 0 -23
ARC Normal -8 -13 8 3 -5 -1 -5 -9
ARC Normal -8 -3 8 13 -5 9 -5 1
ARC Normal -8 7 8 23 0 23 -5 11
LINE Normal 0 23 0 32
SYMATTR 162 �
WINDOW 162 -8 -23 Center 2
SYMATTR Prefix L
WINDOW 0 16 -16 Left 2
SYMATTR Value L
WINDOW 3 16 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
