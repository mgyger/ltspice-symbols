Version 4
SymbolType CELL X-OTA-3080
SYMATTR Description operational transconductance amplifier (OTA) [alternate]
RECTANGLE Normal -32 -48 32 48
LINE Normal 16 -18 -16 -36
LINE Normal -16 -36 -16 36
LINE Normal -16 36 16 18
LINE Normal 16 18 16 -18
LINE Normal -32 -16 -16 -16
LINE Normal -32 16 -16 16
LINE Normal 0 -48 0 -27
LINE Normal 16 0 32 0
SYMATTR 162 -
WINDOW 162 -8 -16 Center 2
SYMATTR 163 +
WINDOW 163 -8 16 Center 2
SYMATTR 164 -
WINDOW 164 16 40 Center 2
SYMATTR 165 +
WINDOW 165 16 -40 Center 2
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel CA3080
WINDOW 38 40 16 Left 2
SYMATTR ModelFile X-OTA.lib
PIN -32 -16 None 8
PINATTR PinName -
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName +
PINATTR SpiceOrder 2
PIN 16 48 None 8
PINATTR PinName V-
PINATTR SpiceOrder 3
PIN 0 -48 None 8
PINATTR PinName Iset
PINATTR SpiceOrder 4
PIN 32 0 None 8
PINATTR PinName Iout
PINATTR SpiceOrder 5
PIN 16 -48 None 8
PINATTR PinName V+
PINATTR SpiceOrder 6
