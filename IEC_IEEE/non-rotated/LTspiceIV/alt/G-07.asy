Version 4
SymbolType CELL G-07
SYMATTR Description buffer, open-drain/collector [alternate]
LINE Normal 32 0 -32 -36
LINE Normal -32 -36 -32 36
LINE Normal -32 36 32 0
LINE Normal -48 0 -32 0
LINE Normal 64 0 32 0
LINE Normal -16 -32 -16 -27
LINE Normal -16 32 -16 27
SYMATTR SpiceModel subckt
WINDOW 38 48 24 Left 2
SYMATTR 162 +
WINDOW 162 -16 -20 Center 2
SYMATTR 163 -
WINDOW 163 -16 20 Center 2
SYMATTR Prefix X
WINDOW 0 48 -24 Left 2
LINE Normal 3 8 11 0
LINE Normal 11 0 3 -8
LINE Normal 3 -8 -5 0
LINE Normal -5 0 3 8
LINE Normal -5 8 11 8
PIN -48 0 None 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 64 0 None 8
PINATTR PinName Y
PINATTR SpiceOrder 2
PIN -16 -32 None 8
PINATTR PinName V+
PINATTR SpiceOrder 3
PIN -16 32 None 8
PINATTR PinName V-
PINATTR SpiceOrder 4
