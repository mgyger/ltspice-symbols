Version 4
SymbolType CELL L-ferrite-bead
SYMATTR Description ferrite bead/choke (ring) [alternate]
LINE Normal 0 -32 0 -10
LINE Normal 0 10 0 32
LINE Normal -18 -1 -10 -15
LINE Normal -10 -15 18 1
LINE Normal 18 1 10 15
LINE Normal 10 15 -18 -1
SYMATTR 162 �
WINDOW 162 -8 -24 Invisible 2
SYMATTR Prefix L_Ferrite_Bead
WINDOW 0 16 24 VRight 2
SYMATTR Value L
WINDOW 3 -16 24 VRight 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
