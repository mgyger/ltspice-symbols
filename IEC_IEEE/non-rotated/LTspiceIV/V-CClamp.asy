Version 4
SymbolType CELL V-CClamp
SYMATTR Description cold-cathode gas-filled tube/lamp, symmetrical; glow/neon lamp/indicator, AC type (scaled)
CIRCLE Normal -32 -32 32 32
SYMATTR 164 �
WINDOW 164 16 8 Center 2
LINE Normal 0 -32 0 -16
CIRCLE Normal -4 -16 4 -8
LINE Normal -8 -16 8 -16
LINE Normal 0 32 0 16
CIRCLE Normal -4 8 4 16
LINE Normal -8 16 8 16
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
