Version 4
SymbolType CELL D-TVS
SYMATTR Description breakdown diode, bidirectional; transient voltage suppressor (TVS)
LINE Normal 0 -32 0 32
LINE Normal 0 0 18 -32
LINE Normal 18 -32 -18 -32
LINE Normal -18 -32 0 0
LINE Normal 0 0 -18 32
LINE Normal -18 32 18 32
LINE Normal 18 32 0 0
LINE Normal -18 -8 -18 0
LINE Normal -18 0 18 0
LINE Normal 18 0 18 8
SYMATTR Prefix D
WINDOW 0 24 -16 Left 2
SYMATTR Value D
WINDOW 3 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
