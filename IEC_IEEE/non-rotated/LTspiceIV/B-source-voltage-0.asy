Version 4
SymbolType CELL B-source-voltage-0
SYMATTR Description zero voltage/tension source; current meter; ammeter
LINE Normal 0 -32 0 32
SYMATTR 162 +
WINDOW 162 -16 -24 Center 2
SYMATTR Prefix V
WINDOW 0 -8 0 Right 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
