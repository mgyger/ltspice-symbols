Version 4
SymbolType GRAPHIC S-actuator-turn
SYMATTR Description actuator, operated by turning (rotary), stay-put/maintained
LINE Normal -10 0 -48 0 Dash
LINE Normal -56 8 -48 8
LINE Normal -48 8 -48 -8
LINE Normal -48 -8 -40 -8
