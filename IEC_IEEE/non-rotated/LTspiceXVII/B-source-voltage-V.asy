Version 4
SymbolType CELL B-source-voltage-V
SYMATTR Description voltage source, voltage controlled (VCVS)
LINE Normal 0 -32 0 32
CIRCLE Normal -16 -16 16 16
SYMATTR 162 +
WINDOW 162 -16 -24 Center 1
SYMATTR 163 +
WINDOW 163 -48 -24 Center 1
SYMATTR Prefix E
WINDOW 0 24 -16 Left 2
SYMATTR Value E
WINDOW 3 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN -32 -32 None 8
PINATTR PinName C+
PINATTR SpiceOrder 3
PIN -32 32 None 8
PINATTR PinName C-
PINATTR SpiceOrder 4
