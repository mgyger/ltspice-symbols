Version 4
SymbolType CELL C
SYMATTR Description capacitor, non-polarized/polar(ized)/bipolar
LINE Normal 0 -32 0 -3
LINE Normal 0 3 0 32
LINE Normal -16 -3 16 -3
LINE Normal -16 3 16 3
SYMATTR 162 +
WINDOW 162 -16 -16 Center 1
SYMATTR Prefix C
WINDOW 0 16 -16 Left 2
SYMATTR Value C
WINDOW 3 16 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
