Version 4
SymbolType CELL X-fuse
SYMATTR Description fuse
LINE Normal 0 -32 0 32
LINE Normal -7 -24 7 -24
LINE Normal 7 -24 7 24
LINE Normal 7 24 -7 24
LINE Normal -7 24 -7 -24
SYMATTR Prefix X
WINDOW 0 16 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 16 16 Left 2
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
