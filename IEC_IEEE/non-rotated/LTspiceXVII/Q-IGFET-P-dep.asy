Version 4
SymbolType CELL Q-IGFET-P-dep
SYMATTR Description P-channel depletion-type insulated-gate field-effect transistor (IGFET), substrate internally connected to source
LINE Normal 0 -16 32 -16
LINE Normal 32 -32 32 0
LINE Normal 32 0 0 0
LINE Normal 32 32 32 16
LINE Normal 32 16 0 16
LINE Normal -32 -16 -6 -16
LINE Normal -6 -16 -6 16
LINE Normal 0 -24 0 24
LINE Normal 6 -5 18 0
LINE Normal 18 0 6 5
SYMATTR Prefix MP
WINDOW 0 48 -16 Left 2
SYMATTR Value PMOS
WINDOW 3 48 16 Left 2
PIN 32 32 None 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN -32 -16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 -32 None 8
PINATTR PinName S
PINATTR SpiceOrder 3
