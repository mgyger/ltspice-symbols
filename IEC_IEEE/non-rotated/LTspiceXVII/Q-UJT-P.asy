Version 4
SymbolType CELL Q-UJT-P
SYMATTR Description unijunction transistor (UJT) with P-type base (NP); complementary unijunction transistor (CUJT)
LINE Normal 0 -16 32 -16
LINE Normal 32 -16 32 -32
LINE Normal 0 16 32 16
LINE Normal 32 16 32 32
LINE Normal -32 16 0 0
LINE Normal 0 24 0 -24
LINE Normal -8 9 -20 10
LINE Normal -20 10 -12 1
SYMATTR Prefix X
WINDOW 0 48 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 48 16 Left 2
PIN -32 16 None 8
PINATTR PinName E
PINATTR SpiceOrder 1
PIN 32 -32 None 8
PINATTR PinName B1
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName B2
PINATTR SpiceOrder 3
