Version 4
SymbolType CELL Q-IGFET-N-enh
SYMATTR Description N-channel enhancement-type insulated-gate field-effect transistor (IGFET), substrate internally connected to source
LINE Normal 0 16 32 16
LINE Normal 32 32 32 0
LINE Normal 32 0 0 0
LINE Normal 32 -32 32 -16
LINE Normal 32 -16 0 -16
LINE Normal -32 16 -6 16
LINE Normal -6 16 -6 -16
LINE Normal 0 10 0 24
LINE Normal 0 -6 0 6
LINE Normal 0 -24 0 -10
LINE Normal 18 5 6 0
LINE Normal 6 0 18 -5
SYMATTR Prefix MN
WINDOW 0 48 -16 Left 2
SYMATTR Value NMOS
WINDOW 3 48 16 Left 2
PIN 32 -32 None 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN -32 16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 32 None 8
PINATTR PinName S
PINATTR SpiceOrder 3
