Version 4
SymbolType CELL D-varicap
SYMATTR Description variable capacitance diode; varactor; varicap
LINE Normal 0 -32 0 32
LINE Normal 0 16 18 -16
LINE Normal 18 -16 -18 -16
LINE Normal -18 -16 0 16
LINE Normal -18 16 18 16
LINE Normal 24 16 24 3
LINE Normal 24 -3 24 -16
LINE Normal 32 3 16 3
LINE Normal 32 -3 16 -3
SYMATTR Prefix D
WINDOW 0 32 -16 Left 2
SYMATTR Value D
WINDOW 3 32 16 Left 2
PIN 0 -32 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
