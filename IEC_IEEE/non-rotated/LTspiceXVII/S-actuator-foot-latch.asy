Version 4
SymbolType GRAPHIC S-actuator-foot-latch
SYMATTR Description actuator, operated by foot (pedal), latching
LINE Normal -10 0 -46 0 Dash
LINE Normal -56 5 -51 8
LINE Normal -51 8 -41 -8
LINE Normal -32 0 -32 -7
LINE Normal -32 -7 -20 0
LINE Normal -20 0 -32 0
