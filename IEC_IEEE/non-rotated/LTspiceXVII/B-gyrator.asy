Version 4
SymbolType CELL B-gyrator
SYMATTR Description gyrator
ARC Normal -28 -12 -4 12 -16 12 -16 -12
LINE Normal -16 -32 -16 32
ARC Normal 4 -12 28 12 16 -12 16 12
LINE Normal 16 -32 16 32
LINE Normal -11 -16 11 -16
LINE Normal 2 -13 11 -16
LINE Normal 11 -16 2 -19
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel gyrator.G.G
SYMATTR Value R=
SYMATTR Value2 R
WINDOW 123 24 16 Left 2
SYMATTR ModelFile B-gyrator.lib
PIN -16 -32 None 8
PINATTR PinName 1+
PINATTR SpiceOrder 1
PIN -16 32 None 8
PINATTR PinName 1-
PINATTR SpiceOrder 2
PIN 16 -32 None 8
PINATTR PinName 2+
PINATTR SpiceOrder 3
PIN 16 32 None 8
PINATTR PinName 2-
PINATTR SpiceOrder 4
