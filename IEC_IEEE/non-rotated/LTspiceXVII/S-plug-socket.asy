Version 4
SymbolType CELL S-plug-socket
SYMATTR Description plug and socket, single pole (scaled 50%)
LINE Normal 32 0 7 0
ARC Normal -7 -7 7 7 0 7 0 -7
LINE Normal -32 0 -16 0
LINE Normal -16 -2 -15 -3
LINE Normal -16 -1 -14 -3
LINE Normal -16 0 -13 -3
LINE Normal -16 1 -12 -3
LINE Normal -16 2 -11 -3
LINE Normal -16 3 -10 -3
LINE Normal -15 3 -9 -3
LINE Normal -14 3 -8 -3
LINE Normal -13 3 -7 -3
LINE Normal -12 3 -6 -3
LINE Normal -11 3 -5 -3
LINE Normal -10 3 -4 -3
LINE Normal -9 3 -3 -3
LINE Normal -8 3 -2 -3
LINE Normal -7 3 -1 -3
LINE Normal -6 3 0 -3
LINE Normal -5 3 0 -2
LINE Normal -4 3 0 -1
LINE Normal -3 3 0 0
LINE Normal -2 3 0 1
LINE Normal -1 3 0 2
LINE Normal -16 -3 0 -3
LINE Normal 0 -3 0 3
LINE Normal 0 3 -16 3
LINE Normal -16 3 -16 -3
SYMATTR Prefix X
WINDOW 0 16 -16 Left 2
SYMATTR InstName S
SYMATTR Value t=-1
SYMATTR ModelFile S-switch.lib
SYMATTR SpiceModel switch.jack1
PIN -32 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 32 0 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
