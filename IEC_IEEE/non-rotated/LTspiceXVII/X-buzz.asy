Version 4
SymbolType CELL X-buzz
SYMATTR Description buzzer
ARC Normal 80 32 16 -32 48 -32 48 32
LINE Normal 48 32 48 -32
LINE Normal 0 32 0 16
LINE Normal 0 16 20 16
LINE Normal 20 -16 0 -16
LINE Normal 0 -16 0 -32
SYMATTR Prefix X
WINDOW 0 56 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 56 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
