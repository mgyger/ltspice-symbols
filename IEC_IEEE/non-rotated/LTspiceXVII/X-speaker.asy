Version 4
SymbolType CELL X-speaker
SYMATTR Description loudspeaker
LINE Normal 8 -16 -8 -16
LINE Normal -8 -16 -8 16
LINE Normal -8 16 8 16
LINE Normal 8 16 24 32
LINE Normal 24 32 24 -32
LINE Normal 24 -32 8 -16
LINE Normal 8 -16 8 16
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
SYMATTR Prefix X
WINDOW 0 32 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 32 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
