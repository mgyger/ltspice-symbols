Version 4
SymbolType CELL S-link
SYMATTR Description connecting link, closed
LINE Normal 0 -32 0 -24
LINE Normal 0 24 0 32
LINE Normal -4 -20 -4 20
LINE Normal 4 20 4 -20
CIRCLE Normal -4 -24 4 -16
CIRCLE Normal -4 16 4 24
SYMATTR Prefix R
WINDOW 0 16 0 Left 2
SYMATTR InstName S
SYMATTR Value 0
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
