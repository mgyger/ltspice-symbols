Version 4
SymbolType CELL V-tetrode
SYMATTR Description electron(ic)/vacuum tube/valve, tetrode (scaled)
ARC Normal -32 -48 32 16 32 -16 -32 -16
LINE Normal -32 -16 -32 0
ARC Normal -32 -32 32 32 -32 0 32 0
LINE Normal 32 0 32 -16
LINE Normal 0 -32 0 -48
LINE Normal 16 -32 -16 -32
LINE Normal 16 0 -16 0 Dash
LINE Normal -16 0 -32 0
LINE Normal 16 -16 -16 -16 Dash
LINE Normal -16 -16 -32 -16
ARC Normal -16 16 16 48 14 24 -16 32
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN 0 -48 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN -32 -16 None 8
PINATTR PinName G2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName G1
PINATTR SpiceOrder 3
PIN -16 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 4
