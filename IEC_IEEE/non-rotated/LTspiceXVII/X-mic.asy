Version 4
SymbolType CELL X-mic
SYMATTR Description microphone
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
CIRCLE Normal -16 -16 16 16
LINE Normal -16 -16 -16 16
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
