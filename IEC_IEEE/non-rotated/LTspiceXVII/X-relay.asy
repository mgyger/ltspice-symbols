Version 4
SymbolType CELL X-relay
SYMATTR Description operating/relay device/coil; electromagnetic actuator (solenoid)
LINE Normal -32 -16 32 -16
LINE Normal 32 -16 32 16
LINE Normal 32 16 -32 16
LINE Normal -32 16 -32 -16
LINE Normal 0 -32 0 -16
LINE Normal 0 16 0 32
SYMATTR Prefix X
WINDOW 0 40 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 40 16 Left 2
PIN 0 -32 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
