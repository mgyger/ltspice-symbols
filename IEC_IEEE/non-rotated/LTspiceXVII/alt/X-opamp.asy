V e r s i o n   4  
 S y m b o l T y p e   C E L L   X - o p a m p  
 S Y M A T T R   D e s c r i p t i o n   o p e r a t i o n a l   a m p l i f i e r   ( o p   a m p ) ;   v o l t a g e   c o m p a r a t o r   [ a l t e r n a t e ]  
 L I N E   N o r m a l   3 2   0   - 3 2   - 3 6  
 L I N E   N o r m a l   - 3 2   - 3 6   - 3 2   3 6  
 L I N E   N o r m a l   - 3 2   3 6   3 2   0  
 L I N E   N o r m a l   0   - 3 2   0   - 1 8  
 L I N E   N o r m a l   0   1 8   0   3 2  
 S Y M A T T R   1 6 2   " 
 W I N D O W   1 6 2   - 2 4   - 1 6   C e n t e r   1  
 S Y M A T T R   1 6 3   +  
 W I N D O W   1 6 3   - 2 4   1 6   C e n t e r   1  
 S Y M A T T R   1 6 4   V " 
 W I N D O W   1 6 4   0   1 2   C e n t e r   0  
 S Y M A T T R   1 6 5   V +  
 W I N D O W   1 6 5   0   - 1 2   C e n t e r   0  
 S Y M A T T R   P r e f i x   X  
 W I N D O W   0   4 0   - 1 6   L e f t   2  
 S Y M A T T R   S p i c e M o d e l   s u b c k t  
 W I N D O W   3 8   4 0   1 6   L e f t   2  
 P I N   - 3 2   1 6   N o n e   8  
 P I N A T T R   P i n N a m e   +  
 P I N A T T R   S p i c e O r d e r   1  
 P I N   - 3 2   - 1 6   N o n e   8  
 P I N A T T R   P i n N a m e   -  
 P I N A T T R   S p i c e O r d e r   2  
 P I N   0   - 3 2   N o n e   8  
 P I N A T T R   P i n N a m e   V +  
 P I N A T T R   S p i c e O r d e r   3  
 P I N   0   3 2   N o n e   8  
 P I N A T T R   P i n N a m e   V -  
 P I N A T T R   S p i c e O r d e r   4  
 P I N   3 2   0   N o n e   8  
 P I N A T T R   P i n N a m e   O U T  
 P I N A T T R   S p i c e O r d e r   5  
 