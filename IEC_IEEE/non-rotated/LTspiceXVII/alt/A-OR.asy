Version 4
SymbolType CELL A-OR
SYMATTR Description OR with complementary outputs (OR/NOR) [alternate]
ARC Normal -72 96 56 -32 49 -1 -8 -32
LINE Normal -8 -32 -40 -32
LINE Normal -40 32 -8 32
ARC Normal -72 -96 56 32 -8 32 49 1
ARC Normal -160 -64 -32 64 -32 64 -32 -64
CIRCLE Normal 37 10 49 22
LINE Normal -48 -16 -34 -16
LINE Normal -48 16 -34 16
LINE Normal 64 16 49 16
LINE Normal 64 -16 35 -16
LINE Normal 0 48 0 32
WINDOW 3 0 64 Center 2
SYMATTR Prefix A
WINDOW 0 0 -64 Center 2
SYMATTR SpiceModel OR
PIN -32 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -48 -16 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN -48 16 None 8
PINATTR PinName 4
PINATTR SpiceOrder 4
PIN -32 32 None 8
PINATTR PinName 5
PINATTR SpiceOrder 5
PIN 64 16 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 64 -16 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 48 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
