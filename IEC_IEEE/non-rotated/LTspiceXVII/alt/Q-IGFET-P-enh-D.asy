Version 4
SymbolType CELL Q-IGFET-P-enh-D
SYMATTR Description P-channel enhancement-type insulated-gate field-effect transistor (IGFET) with inverse (body-drain) diode, substrate internally connected to source [alternate]
LINE Normal 32 32 32 8
LINE Normal 32 -8 32 -32
LINE Normal -8 0 16 0
LINE Normal 16 0 16 -16
LINE Normal 32 -16 -8 -16
LINE Normal 32 16 -8 16
LINE Normal -32 -16 -14 -16
LINE Normal -14 -16 -14 16
LINE Normal -8 10 -8 24
LINE Normal -8 -6 -8 6
LINE Normal -8 -24 -8 -10
LINE Normal -2 -5 10 0
LINE Normal 10 0 -2 5
LINE Normal 32 -8 23 8
LINE Normal 23 8 41 8
LINE Normal 41 8 32 -8
LINE Normal 23 -8 41 -8
SYMATTR Prefix MP
WINDOW 0 48 -16 Left 2
SYMATTR Value PMOS
WINDOW 3 48 16 Left 2
PIN 32 32 None 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN -32 -16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 -32 None 8
PINATTR PinName S
PINATTR SpiceOrder 3
