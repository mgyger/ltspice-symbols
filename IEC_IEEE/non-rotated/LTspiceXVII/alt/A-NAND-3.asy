Version 4
SymbolType CELL A-NAND-3
SYMATTR Description AND with negated output (NAND) [alternate]
LINE Normal -32 32 16 32
ARC Normal -16 -32 48 32 16 32 16 -32
LINE Normal 16 -32 -32 -32
LINE Normal -32 -32 -32 32
CIRCLE Normal 48 -6 60 6
LINE Normal -48 -16 -32 -16
LINE Normal -48 16 -32 16
LINE Normal 64 0 60 0
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel AND
PIN -48 -16 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 64 0 None 8
PINATTR PinName _OUT
PINATTR SpiceOrder 6
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
