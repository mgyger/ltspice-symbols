Version 4
SymbolType CELL A-ST-buf
SYMATTR Description Schmitt-trigger buffer; element with hysteresis; bi-threshold detector [alternate]
LINE Normal 20 0 -32 30
LINE Normal -32 30 -32 -30
LINE Normal -32 -30 20 0
LINE Normal 48 0 20 0
LINE Normal 0 32 0 12
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
LINE Normal -26 8 -14 8
LINE Normal -14 8 -14 -8
LINE Normal -8 -8 -20 -8
LINE Normal -20 -8 -20 8
SYMATTR SpiceModel Schmitt
PIN -32 0 None 8
PINATTR PinName IN
PINATTR SpiceOrder 1
PIN 48 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
