Version 4
SymbolType CELL R
SYMATTR Description resistor [alternate]
LINE Normal 0 -32 0 -24
LINE Normal 0 -24 7 -20
LINE Normal 7 -20 -7 -12
LINE Normal -7 -12 7 -4
LINE Normal 7 -4 -7 4
LINE Normal -7 4 7 12
LINE Normal 7 12 -7 20
LINE Normal -7 20 0 24
LINE Normal 0 24 0 32
SYMATTR 162 +
WINDOW 162 -16 -16 Invisible 1
SYMATTR Prefix R
WINDOW 0 16 -16 Left 2
SYMATTR Value R
WINDOW 3 16 16 Left 2
PIN 0 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
