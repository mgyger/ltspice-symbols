Version 4
SymbolType CELL V-heater
SYMATTR Description heater/filament (scaled) [alternate]
LINE Normal -16 0 -8 0
LINE Normal -8 0 -8 -16
LINE Normal -8 -16 0 -24
LINE Normal 0 -24 8 -16
LINE Normal 8 -16 8 0
LINE Normal 8 0 16 0
SYMATTR Prefix X
WINDOW 0 24 -16 Left 2
SYMATTR SpiceModel subckt
WINDOW 38 24 16 Left 2
PIN -16 0 None 8
PINATTR PinName H1
PINATTR SpiceOrder 1
PIN 16 0 None 8
PINATTR PinName H2
PINATTR SpiceOrder 2
