Version 4
SymbolType CELL R-45
SYMATTR Description resistor (rotated 45�) [alternate]
LINE Normal -32 32 -18 18
LINE Normal -18 18 -20 10
LINE Normal -20 10 -4 14
LINE Normal -4 14 -8 -2
LINE Normal -8 -2 8 2
LINE Normal 8 2 4 -14
LINE Normal 4 -14 20 -10
LINE Normal 20 -10 18 -18
LINE Normal 18 -18 32 -32
SYMATTR 162 +
WINDOW 162 0 -24 Invisible 1
SYMATTR Prefix R
WINDOW 0 -16 -16 Right 2
SYMATTR Value R
WINDOW 3 16 16 Left 2
PIN 32 -32 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -32 32 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
