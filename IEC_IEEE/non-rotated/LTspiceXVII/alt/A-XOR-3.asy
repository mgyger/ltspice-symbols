Version 4
SymbolType CELL A-XOR-3
SYMATTR Description exclusive-OR element (XOR) [alternate]
ARC Normal -172 -64 -44 64 -49 34 -49 -34
ARC Normal -72 96 56 -32 49 -1 -8 -32
LINE Normal -8 -32 -40 -32
ARC Normal -160 -64 -32 64 -37 34 -37 -34
LINE Normal -40 32 -8 32
ARC Normal -72 -96 56 32 -8 32 49 1
LINE Normal -48 -16 -34 -16
LINE Normal -48 16 -34 16
LINE Normal 64 0 48 0
WINDOW 3 0 48 Center 2
SYMATTR Prefix A
WINDOW 0 0 -48 Center 2
SYMATTR SpiceModel XOR
PIN -48 -16 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -48 16 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 0 None 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 64 0 None 8
PINATTR PinName OUT
PINATTR SpiceOrder 7
PIN 0 32 None 8
PINATTR PinName COM
PINATTR SpiceOrder 8
