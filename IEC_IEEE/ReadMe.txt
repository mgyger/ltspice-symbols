IEC/IEEE/ANSI/GOST/DIN/EN/GB Symbols for LTspice


Contains:

100+ graphical symbols for circuit diagrams (schematics) from international
(DIN EN) IEC 60617 standard, adapted for LTspice XVII & IV.

Includes ANSI/IEEE Std 315 zigzag/curved/looped resistor/capacitor/inductor
symbols that can be substituted instead. All symbols contain a description
attribute and some come with subcircuits (timed switches, potentiometers,
gyrator, or photodiodes from ADI).

Note that symbols with 2 vertically aligned pins are rotated (3 times)
to prevent LTspice from rearranging user attributes when rotated again.
Not rotated versions of the symbols can be found in folder non-rotated.


Demos:

0-IEC.asc and 0-alt.asc list most symbols and common combinations,
0-IEC-G.asc and 0-alt-G.asc list single gates (binary logic elements).
They can also be used to copy composite symbols to other schematics.

0-potentiometer.asc demonstrates common pot curves from R-potentiometer.lib
(use Simulate -> Run). See also comments in *.lib files.


Installation:

Copy needed files from LTspiceXVII or LTspiceIV folder into current schematic
folder. Optionally, replace with selected files from corresponding alt folder
(e.g. A-*.asy, X-opamp.asy, X-OTA-*.asy), and rename *-alt.asy files if copied.

Alternatively, for LTspice XVII only, add the folders in desired order to
Symbol Search Path in Tools -> Control Panel -> Sym. & Lib. Search Paths.


License:

Unless stated otherwise: public domain
https://creativecommons.org/publicdomain/zero/1.0/

