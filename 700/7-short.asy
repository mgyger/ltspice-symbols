Version 4
SymbolType CELL 7-short
SYMATTR Description symbol pin short circuit (wire jumper)
LINE Normal 0 0 0 16
SYMATTR Prefix R
SYMATTR Value 0
SYMATTR InstName SHORT
PIN 0 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 0 16 None 8
PINATTR PinName 2
PINATTR SpiceOrder 2
