Version 4
SymbolType CELL 7-SNPN
SYMATTR Description split-emitter NPN transistor
LINE Normal -16 0 0 0
LINE Normal 16 -32 16 -16
LINE Normal 16 -16 0 0
LINE Normal 16 16 0 0
LINE Normal 16 32 16 28
LINE Normal 16 28 0 12
LINE Normal 0 -16 0 16
LINE Normal 5 8 11 11
LINE Normal 11 11 8 5
LINE Normal 5 20 11 23
LINE Normal 11 23 8 17
SYMATTR Prefix X
WINDOW 0 16 -8 Left 1
SYMATTR InstName Q
SYMATTR SpiceModel SNPN
PIN 16 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -16 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 16 16 None 8
PINATTR PinName E1
PINATTR SpiceOrder 3
PIN 16 32 None 8
PINATTR PinName E2
PINATTR SpiceOrder 4
