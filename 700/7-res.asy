Version 4
SymbolType CELL 7-res
SYMATTR Description resistor
LINE Normal 0 0 0 6
LINE Normal 0 42 0 48
LINE Normal -5 6 5 6
LINE Normal 5 6 5 42
LINE Normal 5 42 -5 42
LINE Normal -5 42 -5 6
SYMATTR 162 +
WINDOW 162 -12 6 Invisible 1
SYMATTR Value 1
WINDOW 3 9 32 Left 1
SYMATTR Prefix R
WINDOW 0 9 16 Left 1
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 48 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
