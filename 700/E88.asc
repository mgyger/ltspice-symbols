Version 4
SHEET 1 0 0
FLAG 480 432 0
FLAG 528 432 0
SYMBOL 7-term 560 208 R0
SYMATTR InstName TERM01
SYMATTR 166 VP
SYMBOL 7-term 560 320 R0
SYMATTR InstName TERM02
SYMATTR 166 ((R1+R2)/R2)VBE
SYMBOL 7-NPN1 496 384 M0
SYMATTR InstName Q1
SYMBOL 7-NPN3 512 288 R0
SYMATTR InstName Q2
SYMBOL 7-RB 528 336 R0
SYMATTR InstName RB1
SYMATTR Value 5
SYMBOL 7-RB 528 384 R0
SYMATTR InstName RB2
SYMATTR Value 10
SYMBOL 7-RB 480 224 R0
SYMATTR InstName RB3
SYMATTR Value 40
SYMBOL 7-SUB 480 432 R0
WIRE 480 208 480 224
WIRE 480 208 528 208
WIRE 480 272 480 288
WIRE 480 288 480 352
WIRE 480 288 496 288
WIRE 480 416 480 432
WIRE 512 384 528 384
WIRE 528 208 528 256
WIRE 528 208 560 208
WIRE 528 320 528 336
WIRE 528 320 560 320
