Version 4
SymbolType CELL 7-JCAP
SYMATTR Description junction capacitor; variable capacitance diode
LINE Normal 0 32 0 0
LINE Normal 0 8 -9 24
LINE Normal -9 24 9 24
LINE Normal 9 24 0 8
LINE Normal 9 8 -9 8
LINE Normal -16 8 -16 14
LINE Normal -16 18 -16 24
LINE Normal -22 14 -10 14
LINE Normal -22 18 -10 18
SYMATTR 162 +
WINDOW 162 -22 8 Center 0
SYMATTR Prefix X
WINDOW 0 12 16 Left 1
SYMATTR InstName JCAP
SYMATTR SpiceModel JCAP
PIN 0 0 None 8
PINATTR PinName K+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName A-
PINATTR SpiceOrder 2
