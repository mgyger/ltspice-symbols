Version 4
SymbolType BLOCK 7-LPNP
SYMATTR Description large lateral PNP transistor
LINE Normal -16 0 0 0
LINE Normal 16 -32 16 -16
LINE Normal 16 -16 0 0
LINE Normal 16 32 16 16
LINE Normal 16 16 0 0
LINE Normal -1 -15 0 -16
LINE Normal -1 -14 1 -16
LINE Normal -1 -13 1 -15
LINE Normal -1 -12 1 -14
LINE Normal -1 -11 1 -13
LINE Normal -1 -10 1 -12
LINE Normal -1 -9 1 -11
LINE Normal -1 -8 1 -10
LINE Normal -1 -7 1 -9
LINE Normal -1 -6 1 -8
LINE Normal -1 -5 1 -7
LINE Normal -1 -4 1 -6
LINE Normal -1 -3 1 -5
LINE Normal -1 -2 1 -4
LINE Normal -1 -1 1 -3
LINE Normal -1 0 1 -2
LINE Normal -1 1 1 -1
LINE Normal -1 2 1 0
LINE Normal -1 3 1 1
LINE Normal -1 4 1 2
LINE Normal -1 5 1 3
LINE Normal -1 6 1 4
LINE Normal -1 7 1 5
LINE Normal -1 8 1 6
LINE Normal -1 9 1 7
LINE Normal -1 10 1 8
LINE Normal -1 11 1 9
LINE Normal -1 12 1 10
LINE Normal -1 13 1 11
LINE Normal -1 14 1 12
LINE Normal -1 15 1 13
LINE Normal -1 16 1 14
LINE Normal 0 16 1 15
LINE Normal -1 -16 1 -16
LINE Normal 1 -16 1 16
LINE Normal 1 16 -1 16
LINE Normal -1 16 -1 -16
LINE Normal 11 -8 5 -5
LINE Normal 5 -5 8 -11
WINDOW 0 16 -8 Left 1
SYMATTR InstName Q
PIN 16 32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -16 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 16 -32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
