Version 4
SymbolType BLOCK 7-RBMV
SYMATTR Description voltage dependent multiple base resistor
LINE Normal 0 0 0 6
LINE Normal 0 42 0 48
LINE Normal -5 6 5 6
LINE Normal 5 6 5 42
LINE Normal 5 42 -5 42
LINE Normal -5 42 -5 6
SYMATTR 162 +
WINDOW 162 -12 6 Invisible 1
WINDOW 0 9 16 Left 1
SYMATTR SpiceLine a=
SYMATTR SpiceLine2 1
WINDOW 40 9 32 Left 1
SYMATTR InstName RBMV
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 48 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
