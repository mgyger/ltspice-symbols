Version 4
SymbolType BLOCK 7-PAD
SYMATTR Description (bonding) pad
LINE Normal 0 0 12 0
LINE Normal 12 -4 20 -4
LINE Normal 20 -4 20 4
LINE Normal 20 4 12 4
LINE Normal 12 4 12 -4
WINDOW 0 24 0 Left 1
SYMATTR InstName PAD_
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
