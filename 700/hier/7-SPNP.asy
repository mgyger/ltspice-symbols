Version 4
SymbolType BLOCK 7-SPNP
SYMATTR Description lateral split-collector PNP transistor
LINE Normal -16 0 0 0
LINE Normal 16 -32 16 -16
LINE Normal 16 -16 0 0
LINE Normal 16 16 0 0
LINE Normal 16 32 16 28
LINE Normal 16 28 0 12
LINE Normal 0 -16 0 16
LINE Normal 11 -8 5 -5
LINE Normal 5 -5 8 -11
WINDOW 0 16 -8 Left 1
SYMATTR InstName Q
PIN 16 16 None 8
PINATTR PinName C1
PINATTR SpiceOrder 1
PIN 16 32 None 8
PINATTR PinName C2
PINATTR SpiceOrder 2
PIN -16 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 3
PIN 16 -32 None 8
PINATTR PinName E
PINATTR SpiceOrder 4
