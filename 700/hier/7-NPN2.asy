Version 4
SymbolType BLOCK 7-NPN2
SYMATTR Description NPN transistor with 2 emitters
LINE Normal 16 28 0 12
LINE Normal -16 0 0 0
LINE Normal 16 -32 16 -16
LINE Normal 16 -16 0 0
LINE Normal 16 32 16 16
LINE Normal 16 16 0 0
LINE Normal 0 -16 0 16
LINE Normal 5 8 11 11
LINE Normal 11 11 8 5
WINDOW 0 16 -8 Left 1
SYMATTR InstName Q
LINE Normal 5 20 11 23
LINE Normal 11 23 8 17
PIN 16 -32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -16 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 16 32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
