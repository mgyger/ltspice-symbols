Version 4
SymbolType BLOCK 7-REP
SYMATTR Description epi(taxial) pinch resistor
LINE Normal 0 0 0 6
LINE Normal 0 42 0 48
LINE Normal -5 6 5 6
LINE Normal 5 6 5 42
LINE Normal 5 42 -5 42
LINE Normal -5 42 -5 6
LINE Normal -12 42 -12 36
LINE Normal -12 36 12 12
LINE Normal -12 49 -11 48
LINE Normal -12 50 -10 48
LINE Normal -11 50 -9 48
LINE Normal -10 50 -8 48
LINE Normal -9 50 -7 48
LINE Normal -8 50 -6 48
LINE Normal -7 50 -5 48
LINE Normal -6 50 -4 48
LINE Normal -5 50 -3 48
LINE Normal -4 50 -2 48
LINE Normal -3 50 -1 48
LINE Normal -2 50 0 48
LINE Normal -1 50 1 48
LINE Normal 0 50 2 48
LINE Normal 1 50 3 48
LINE Normal 2 50 4 48
LINE Normal 3 50 5 48
LINE Normal 4 50 6 48
LINE Normal 5 50 7 48
LINE Normal 6 50 8 48
LINE Normal 7 50 9 48
LINE Normal 8 50 10 48
LINE Normal 9 50 11 48
LINE Normal 10 50 12 48
LINE Normal 11 50 12 49
LINE Normal -12 48 12 48
SYMATTR 162 +
WINDOW 162 -12 6 Invisible 1
WINDOW 0 9 24 Left 1
SYMATTR InstName REP
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
