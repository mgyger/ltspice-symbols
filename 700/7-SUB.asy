Version 4
SymbolType CELL 7-SUB
SYMATTR Description substrate; functional equipotential bonding
LINE Normal -12 1 -11 0
LINE Normal -12 2 -10 0
LINE Normal -11 2 -9 0
LINE Normal -10 2 -8 0
LINE Normal -9 2 -7 0
LINE Normal -8 2 -6 0
LINE Normal -7 2 -5 0
LINE Normal -6 2 -4 0
LINE Normal -5 2 -3 0
LINE Normal -4 2 -2 0
LINE Normal -3 2 -1 0
LINE Normal -2 2 0 0
LINE Normal -1 2 1 0
LINE Normal 0 2 2 0
LINE Normal 1 2 3 0
LINE Normal 2 2 4 0
LINE Normal 3 2 5 0
LINE Normal 4 2 6 0
LINE Normal 5 2 7 0
LINE Normal 6 2 8 0
LINE Normal 7 2 9 0
LINE Normal 8 2 10 0
LINE Normal 9 2 11 0
LINE Normal 10 2 12 0
LINE Normal 11 2 12 1
LINE Normal -12 0 12 0
SYMATTR Prefix X
SYMATTR InstName SUB
SYMATTR SpiceModel SUB
SYMATTR ModelFile 70025.lib
PIN 0 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
