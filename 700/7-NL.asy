Version 4
SymbolType CELL 7-NL
SYMATTR Description N-well/layer
LINE Normal -2 -10 0 -16
LINE Normal 0 -16 2 -10
LINE Normal 0 0 0 -16
SYMATTR Prefix X
SYMATTR InstName NL
SYMATTR SpiceModel NL
WINDOW 38 6 -10 Left 1
PIN 0 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
