Version 4
SymbolType CELL 7-njfet
SYMATTR Description N-channel junction-gate field-effect transistor (JFET)
LINE Normal 16 0 32 0
LINE Normal 0 16 32 16
LINE Normal 16 -8 16 24
LINE Normal 5 14 11 16
LINE Normal 11 16 5 18
SYMATTR Prefix JN
WINDOW 0 36 0 Left 1
SYMATTR InstName Q
SYMATTR Value NJF
WINDOW 3 36 16 Left 1
PIN 32 0 None 8
PINATTR PinName D
PINATTR SpiceOrder 1
PIN 0 16 None 8
PINATTR PinName G
PINATTR SpiceOrder 2
PIN 32 16 None 8
PINATTR PinName S
PINATTR SpiceOrder 3
