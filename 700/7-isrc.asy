Version 4
SymbolType CELL 7-isrc
SYMATTR Description independent current source
LINE Normal -16 32 16 32
LINE Normal 0 0 0 16
LINE Normal 0 48 0 64
CIRCLE Normal -16 16 16 48
SYMATTR 162 +
WINDOW 162 -8 8 Center 1
SYMATTR Prefix I
WINDOW 0 20 24 Left 1
SYMATTR Value 1m
WINDOW 3 20 40 Left 1
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 64 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
