Version 4
SymbolType CELL 7-xtal
SYMATTR Description piezoelectric crystal/xtal
LINE Normal 0 0 0 8
LINE Normal 0 24 0 32
LINE Normal -12 8 12 8
LINE Normal -12 24 12 24
LINE Normal -18 11 18 11
LINE Normal 18 11 18 21
LINE Normal 18 21 -18 21
LINE Normal -18 21 -18 11
SYMATTR 162 +
WINDOW 162 -21 8 Invisible 1
SYMATTR Prefix C
WINDOW 0 24 16 Left 1
SYMATTR InstName X
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
