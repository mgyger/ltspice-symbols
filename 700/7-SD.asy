Version 4
SymbolType CELL 7-SD
SYMATTR Description Schottky diode
LINE Normal 0 0 0 32
LINE Normal 0 24 9 8
LINE Normal 9 8 -9 8
LINE Normal -9 8 0 24
LINE Normal -5 20 -9 20
LINE Normal -9 20 -9 24
LINE Normal -9 24 9 24
LINE Normal 9 24 9 28
LINE Normal 9 28 5 28
SYMATTR Prefix D
WINDOW 0 12 16 Left 1
SYMATTR InstName SD
SYMATTR SpiceModel SD
WINDOW 3 -12 24 Right 1
PIN 0 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
