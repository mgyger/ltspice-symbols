Version 4
SymbolType CELL 7-SUB1
SYMATTR Description substrate (alternate)
LINE Normal -2 -10 0 -16
LINE Normal 0 -16 2 -10
LINE Normal 0 0 0 -16
SYMATTR Prefix X
SYMATTR InstName SUB
SYMATTR SpiceModel SUB1
WINDOW 38 6 -10 Left 1
PIN 0 0 None 8
PINATTR PinName 1
PINATTR SpiceOrder 1
