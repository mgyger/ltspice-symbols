Version 4
SymbolType CELL 7-CU
SYMATTR Description cross-under (resistor)
LINE Normal 0 0 0 4
LINE Normal 0 28 0 32
LINE Normal -5 4 5 4
LINE Normal 5 4 5 28
LINE Normal 5 28 -5 28
LINE Normal -5 28 -5 4
SYMATTR 162 +
WINDOW 162 -12 6 Invisible 1
SYMATTR Prefix RCU
WINDOW 0 9 8 Left 1
SYMATTR InstName RCU
SYMATTR SpiceModel CU
SYMATTR Value 1
WINDOW 3 9 24 Left 1
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
