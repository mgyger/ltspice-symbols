Version 4
SymbolType CELL 7-PNP1
SYMATTR Description lateral PNP transistor with 1 collector
LINE Normal -16 0 0 0
LINE Normal 16 -32 16 -16
LINE Normal 16 -16 0 0
LINE Normal 16 32 16 16
LINE Normal 16 16 0 0
LINE Normal 0 -16 0 16
LINE Normal 11 -8 5 -5
LINE Normal 5 -5 8 -11
SYMATTR Prefix X
WINDOW 0 16 -8 Left 1
SYMATTR InstName Q
SYMATTR SpiceModel PNP1
PIN 16 32 None 8
PINATTR PinName C
PINATTR SpiceOrder 1
PIN -16 0 None 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 16 -32 None 8
PINATTR PinName E
PINATTR SpiceOrder 3
