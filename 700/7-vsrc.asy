Version 4
SymbolType CELL 7-vsrc
SYMATTR Description independent voltage/tension source
LINE Normal 0 0 0 64
CIRCLE Normal -16 16 16 48
SYMATTR 162 +
WINDOW 162 -8 8 Center 1
SYMATTR Prefix V
WINDOW 0 20 24 Left 1
SYMATTR Value 0
WINDOW 3 20 40 Left 1
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 64 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
