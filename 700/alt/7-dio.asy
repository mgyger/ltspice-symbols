Version 4
SymbolType CELL 7-dio
SYMATTR Description semiconductor diode [alternate]
LINE Normal 0 0 0 8
LINE Normal 0 24 0 32
LINE Normal 0 24 9 8
LINE Normal 9 8 -9 8
LINE Normal -9 8 0 24
LINE Normal -9 24 9 24
SYMATTR 162 +
WINDOW 162 -16 8 Invisible 1
SYMATTR Prefix D
WINDOW 0 12 8 Left 1
SYMATTR Value D
WINDOW 3 12 24 Left 1
PIN 0 0 None 8
PINATTR PinName A+
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName K-
PINATTR SpiceOrder 2
