Version 4
SymbolType CELL 7-RBP
SYMATTR Description base pinch resistor, polarized [alternate]
LINE Normal 0 0 0 6
LINE Normal 0 6 5 9
LINE Normal 5 9 -5 15
LINE Normal -5 15 5 21
LINE Normal 5 21 -5 27
LINE Normal -5 27 5 33
LINE Normal 5 33 -5 39
LINE Normal -5 39 0 42
LINE Normal 0 42 0 48
LINE Normal -12 42 -12 36
LINE Normal -12 36 12 12
SYMATTR 162 +
WINDOW 162 -12 6 Center 1
SYMATTR Prefix X
WINDOW 0 9 24 Left 1
SYMATTR InstName RBP
SYMATTR SpiceModel RBP
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 48 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
