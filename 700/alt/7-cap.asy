Version 4
SymbolType CELL 7-cap
SYMATTR Description capacitor (polarized) [alternate]
LINE Normal 0 0 0 13
LINE Normal 0 19 0 32
LINE Normal -12 13 12 13
ARC Normal -32 19 32 83 12 21 -12 21
SYMATTR 162 +
WINDOW 162 -6 7 Center 1
SYMATTR Prefix C
WINDOW 0 16 8 Left 1
SYMATTR Value 1n
WINDOW 3 16 24 Left 1
PIN 0 0 None 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 32 None 8
PINATTR PinName -
PINATTR SpiceOrder 2
