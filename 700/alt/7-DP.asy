Version 4
SymbolType CELL 7-DP
SYMATTR Description (bonding pad) ESD protection diode [alternate]
LINE Normal 0 32 0 24
LINE Normal 0 8 0 0
LINE Normal 0 8 -9 24
LINE Normal -9 24 9 24
LINE Normal 9 24 0 8
LINE Normal -9 8 9 8
LINE Normal -12 33 -11 32
LINE Normal -12 34 -10 32
LINE Normal -11 34 -9 32
LINE Normal -10 34 -8 32
LINE Normal -9 34 -7 32
LINE Normal -8 34 -6 32
LINE Normal -7 34 -5 32
LINE Normal -6 34 -4 32
LINE Normal -5 34 -3 32
LINE Normal -4 34 -2 32
LINE Normal -3 34 -1 32
LINE Normal -2 34 0 32
LINE Normal -1 34 1 32
LINE Normal 0 34 2 32
LINE Normal 1 34 3 32
LINE Normal 2 34 4 32
LINE Normal 3 34 5 32
LINE Normal 4 34 6 32
LINE Normal 5 34 7 32
LINE Normal 6 34 8 32
LINE Normal 7 34 9 32
LINE Normal 8 34 10 32
LINE Normal 9 34 11 32
LINE Normal 10 34 12 32
LINE Normal 11 34 12 33
LINE Normal -12 32 12 32
SYMATTR 162 +
SYMATTR Prefix X
WINDOW 0 12 16 Left 1
SYMATTR InstName DP
SYMATTR SpiceModel DP
PIN 0 0 None 8
PINATTR PinName K+
PINATTR SpiceOrder 1
